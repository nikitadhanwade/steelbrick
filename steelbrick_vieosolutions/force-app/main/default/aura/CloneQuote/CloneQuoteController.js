({
    doInit : function(component, event, helper) {
         debugger;
        var qouteRecordId= component.get("v.recordId");
       
        if(qouteRecordId !== null && qouteRecordId !== '' && qouteRecordId !== undefined){
            var action = component.get("c.cloneQuote");
             //var inputsel = component.find("InputSelectDynamic");
            action.setParams({"recordId":qouteRecordId}); 
            var opts=[];
            action.setCallback(this, function(response){    
                debugger;
                 var state = response.getState();  
                if(state == "SUCCESS"){
                    console.log('Sucess');
                     //var forecasts = component.get("v.recordId");
                    //forecasts = [];
                    //forecasts = forecasts.concat(response.getReturnValue());
                     //component.set("v.quote", forecasts);
                   /* var quote = [{ class: "optionClass", label: "In Review", value: "In Review"},
                               { class: "optionClass", label: "Est", value: "Est" },
                              ];  
                    
                     quote = quote.concat(response.getReturnValue());*/
        
                   component.set('v.quote', response.getReturnValue()); 
                    
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);                  
                        component.set("v.errorMessages",errors[0].message);
                    }
                }else {
                    console.log("Unknown error");
                }
            }); 
            $A.enqueueAction(action); 
        }
    }  ,
    closeModel: function(component, event, helper) {
        debugger;
         $A.get("e.force:closeQuickAction").fire();  
   },
    save: function(component, event, helper) {
         debugger;
        var quote =component.get("v.quote");
        quote.Name = component.find("name");
        var value = quote.Name.get("v.value");
        // Is input numeric?
        if (value === "" || value === null) {
            // Set error
            quote.Name.set("v.errors", [{message:"please enter name" + value}]);
        } else {
            // Clear error
            quote.Name.set("v.errors", null);
        }
        //var ContactId = component.find("ConId").get("v.value");
    	quote.BillingCity = component.find("BillingCity").get("v.value");
        quote.BillingCountry = component.find("BillingCountry").get("v.value");
        quote.BillingName = component.find("BillingName").get("v.value");
        quote.BillingPostalCode = component.find("BillingPostalCode").get("v.value");
        quote.BillingState = component.find("BillingState").get("v.value");
        quote.BillingStreet = component.find("BillingStreet").get("v.value");
        quote.Description = component.find("Description").get("v.value");
        quote.Email = component.find("Email").get("v.value");
        quote.Phone = component.find("Phone").get("v.value");
        quote.ShippingCity = component.find("ShippingCity").get("v.value");
        quote.ShippingCountry = component.find("ShippingCountry").get("v.value");
        quote.ShippingName = component.find("ShippingName").get("v.value");
        quote.ShippingPostalCode = component.find("ShippingPostalCode").get("v.value");
        quote.ShippingState = component.find("ShippingState").get("v.value");
        quote.ShippingStreet = component.find("ShippingStreet").get("v.value");
        //quote.OpportunityId = component.find("OpportunityId").get("v.value");
        //quote.AccountId = component.find("AccountId").get("v.value");
        quote.Product2Id = component.find("Product2Id").get("v.value");
        //quote.Description = component.find("Description").get("v.value");
        quote.Discount = component.find("Discount").get("v.value");
        quote.ListPrice = component.find("ListPrice").get("v.value"); 
        quote.Quantity = component.find("Quantity").get("v.value");
        quote.Subtotal = component.find("Subtotal").get("v.value");
        quote.TotalPrice = component.find("TotalPrice").get("v.value");
        quote.UnitPrice = component.find("UnitPrice").get("v.value");
        //quote.Status = component.find("Status").get("v.value");
        quote.GrandTotal = component.find("GrandTotal").get("v.value");
        //quote.ExpirationDate = component.find("expirationdate").get("v.value");
        var ExpirationDate= component.find("expirationdate").get("v.value");
        //quote.ContactId = component.get("v.recordId");
        //quote.Source_Quote__c = component.find("source_quote__c").get("v.value");
        var SourceQuotec = component.find("source_quote__c").get("v.value");
        
        console.log('quote ::',quote);
        if(value != "" && value != null){
       var action = component.get("c.saveData");
        debugger;
        action.setParams({quote:quote,
                          SourceQuotec:SourceQuotec,
                          ExpirationDate:ExpirationDate});
        action.setCallback(this, function(resp) {
		debugger;
            if (resp.getState() === "SUCCESS") { 
                var newQuoteId = resp.getReturnValue()
                var getInstance= "https://"+location.hostname+"/lightning/r/Quote/"+newQuoteId+"/view";
                                var urlEvent = $A.get("e.force:navigateToURL"); 
                                urlEvent.setParams({ 
                                    "url": getInstance
                                }); 
                                urlEvent.fire();
            }
            
        });

        $A.enqueueAction(action);
        }
        
    }
})