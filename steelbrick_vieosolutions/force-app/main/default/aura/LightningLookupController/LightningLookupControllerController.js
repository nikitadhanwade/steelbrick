({
    doInit : function(cmp, event, helper) {
        helper.doInitHelper(cmp);        
    },
    /**
     * Search an SObject for a match
     */
    search : function(cmp, event, helper) {
        helper.doSearch(cmp);        
    },
 select: function(cmp, event, helper) {
     debugger;
        helper.handleSelection(cmp, event);
    },
    /**
     * Select an SObject from a list
     */
    
     
    /**
     * Clear the currently selected SObject
     */
    clear: function(cmp, event, helper) {
        helper.clearSelection(cmp);    
    },
    selectItem: function(cmp, event, helper) {
        helper.setSelectedItem(cmp);    
    }
})