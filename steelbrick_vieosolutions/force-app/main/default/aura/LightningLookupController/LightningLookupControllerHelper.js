({
    /**
     * Perform the SObject search via an Apex Controller
     */
    doSearch : function(cmp) {
        // Get the search string, input element and the selection container
        var searchString = cmp.get('v.SearchKeyWord');
        var inputElement = cmp.find('lookup');
        var lookupList = cmp.find('lookuplist');
        
        // Clear any errors and destroy the old lookup items container
        inputElement.set('v.errors', null);
        
        // We need at least 2 characters for an effective search
        if (searchString === null || searchString === undefined || searchString.length < 3)
        {
            // Hide the lookuplist
            $A.util.removeClass(lookupList, 'slds-show');
            $A.util.addClass(lookupList, 'slds-hide');
            return;
        }
        
        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');
        $A.util.addClass(lookupList, 'slds-show');
        // Get the API Name
        var query= cmp.get("v.query");
        var isSOSL = cmp.get('v.isSOSL');
        // Create an Apex action
        var action = cmp.get('c.fetchAccount');
        
        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();
        
        // Set the parameters
        action.setParams({ "searchString" : searchString, "query" : query,"isSOSL":isSOSL});
        
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();
                
                // If we have no matches, return nothing
                if (matches.length == 0)
                {
                    cmp.set('v.listOfSearchRecords', null);
                    if(inputElement !== null && inputElement !== undefined && inputElement !== ''){
                        
                        $A.util.removeClass(lookupList, 'slds-show');
                        $A.util.addClass(lookupList, 'slds-hide');
                    }
                    return;
                }
                else{
                    if(inputElement !== null && inputElement !== undefined && inputElement !== ''){
                        inputElement.set("v.errors", null);
                    }
                    // Store the results
                    
                    for(var iCount = 0; iCount < matches.length; iCount++){
                        var tempName = matches[iCount].Name;
                        if(tempName !== null && tempName !== '' && tempName !== undefined){
                            if(tempName.length > 30){
                                matches[iCount].Title = matches[iCount].Name.substr(0,30) + '..';
                            }
                            else{
                                matches[iCount].Title = matches[iCount].Name;
                            }
                        }
                    }
                    cmp.set('v.listOfSearchRecords', matches);
                }
                
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();
                
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        if(inputElement !== null && inputElement !== undefined && inputElement !== ''){
                            inputElement.set("v.errors", errors[0].message);
                        }
                    }
                }
                else
                {
                    if(inputElement !== null && inputElement !== undefined && inputElement !== ''){
                        inputElement.set("v.errors", 'Unknown error.');
                    }
                }
            }
        });
        
        // Enqueue the action                  
        $A.enqueueAction(action);                
    },
    /**
     * Handle the Selection of an Item
     */
    handleSelection : function(cmp, event) {
        debugger;
        // Resolve the Object Id from the events Element Id (this will be the <a> tag)
        var uniqueId = event.currentTarget.getAttribute("data-Unique-Id");
        var endDate = event.currentTarget.getAttribute("data-end-date");
        var startDate = event.currentTarget.getAttribute("data-start-date");
        var careFlag = event.currentTarget.getAttribute("data-care-flag");
        var riskFlag = event.currentTarget.getAttribute("data-risk-flag");
        var riskAmount = event.currentTarget.getAttribute("data-risk-amount");
        var houseHoldId = event.currentTarget.getAttribute("data-accountId-flag");
        var recordId = event.currentTarget.getAttribute("data-id");
        var displayRecord = event.currentTarget.getAttribute("data-record");
        if(houseHoldId !== null && houseHoldId !== undefined && houseHoldId !== ""){
            cmp.set("v.houseHoldId",houseHoldId);
        }
        if(careFlag !== null && careFlag!== undefined && careFlag !=="" 
           && (careFlag === true || careFlag === "true")){
            cmp.set("v.careFlag",true);
        }
        if(riskFlag !== null && riskFlag!== undefined && riskFlag !=="" 
           && (riskFlag === true || riskFlag === "true")){
            cmp.set("v.riskFlag",true);
        }
        if(riskAmount !== null && riskAmount !== undefined && riskAmount !=="" 
           && riskAmount >0){
            cmp.set("v.riskAmount",riskAmount);
        }
        if(endDate !== null && endDate!== undefined && endDate !==""){
            cmp.set("v.startDate",startDate.slice(0,10));
            cmp.set("v.endDate",endDate.slice(0,10));
        }
        if(uniqueId !== null && uniqueId !== undefined && uniqueId !== ""){
            cmp.set("v.uniqueId",uniqueId);
        }
        var preference=true;
        
            var action = cmp.get("c.getContact");
            action.setParams({ payeeId : recordId});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state=="SUCCESS"){
                    var result= response.getReturnValue();
                    preference = result;
                    cmp.set("v.acknowledgement ",preference);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                            if(errors[0].message !== null && errors[0].message !== undefined && errors[0].message !== ""){
                                var errorSplit =errors[0].message.split(",");
                                if(errorSplit !== null && errorSplit !== undefined){
                                    cmp.set("v.MessageText",errorSplit[1]);
                                    cmp.set("v.Message",errorSplit[1]);
                                }
                            }
                        }
                    }else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action); 
        
        cmp.set("v.displayRecord",displayRecord);  
        cmp.set("v.recordID",recordId);  
        cmp.set("v.SearchKeyWord",'');  
    },
    
    /**
     * Clear the Selection
     */
    clearSelection : function(cmp) {
        debugger;
        // Create the ClearLookupId event
        /*var clearEvent = cmp.getEvent("clearLookupIdEvent");
         
        // Fire the event
        clearEvent.fire();
 		*/
        // Clear the Searchstring
        cmp.set("v.searchString", '');
        
        // Hide the Lookup pill
        var lookupPill = cmp.find("lookup-pill");
        $A.util.addClass(lookupPill, 'slds-hide');
        
        // Show the Input Element
        var inputElement = cmp.find('lookup');
        $A.util.addClass(inputElement, 'slds-show');
        
        // Lookup Div has no selection
        var inputElement = cmp.find('lookup-div');
        $A.util.removeClass(inputElement, 'slds-has-selection');
        cmp.set("v.displayRecord","");
        cmp.set("v.recordID","");
        cmp.set("v.listOfSearchRecords","");
        cmp.set("v.SearchKeyWord",'');
        cmp.set("v.message", "");
    },
    setSelectedItem : function(cmp){
        var displayRecord = cmp.get("v.displayRecord");
        if(displayRecord !== null && displayRecord !== undefined && displayRecord !==""){
            // Hide the Lookup List
            var lookupList = cmp.find("lookuplist");
            $A.util.addClass(lookupList, 'slds-hide');
            
            // Hide the Input Element
            var inputElement = cmp.find('lookup');
            $A.util.addClass(inputElement, 'slds-hide');
            
            // Show the Lookup pill
            var lookupPill = cmp.find("lookup-pill");
            $A.util.removeClass(lookupPill, 'slds-hide');
            $A.util.addClass(lookupPill, 'slds-show');
            
            // Lookup Div has selection
            var inputElement = cmp.find('lookup-div');
            $A.util.addClass(inputElement, 'slds-has-selection');
        }
        else{
            var inputElement = cmp.find('lookup-div');
            $A.util.removeClass(inputElement, 'slds-has-selection');
        }
    },
    doInitHelper :  function(cmp){
        var displayRecord = cmp.get("v.displayRecord");
        if(displayRecord === null || displayRecord === undefined || displayRecord ===""){
            cmp.set("v.displayRecord","");
        }
    }
})