({
    handleSave : function(component, event, helper) {
        debugger;
        var maxStringSize = 6000000; //Maximum String size is 6,000,000 characters
        var maxFileSize = 19242880; //After Base64 Encoding, this is the max file size
        var chunkSize = 950000; //Maximum Javascript Remoting message size is 1,000,000 characters
        var attachment;
        var attachmentName;
        var fileSize;
        var positionIndex;
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        var messageToDisplay = 'Record Save successfully found for Serial number';
        var recordId = component.get("v.recordId");
        var GroupId = component.get("v.GroupId");
        component.set('v.columns', [
            {label: 'Action', type: 'button', initialWidth: 150, typeAttributes:
             { label: { fieldName: 'actionLabel'}, title: 'Click to Edit', name: 'view_details', disabled: {fieldName: 'actionDisabled'}, class: 'btn_next'}},
            {label: 'CPQ Fields', fieldName: 'SalesforceFieldName', type: 'text', cellAttributes: { class: { fieldName: 'UnmappedCSS' }}},
            {label: 'Import CSV file fields', fieldName: 'fieldName', type: 'text'}   
        ]);
        
        if (file !== undefined && file !== '' && file !== null) {
            var fileNameTest = fileInput.value;
            var ext = fileNameTest.substring(fileNameTest.lastIndexOf('.'));
            if(ext == ".csv"){
                if (file.size <= maxFileSize) {   
                    var  attachmentName =file.name;
                    console.log('attachmentName>>>>>'+attachmentName);
                    var fileReader = new FileReader();
                    fileReader.onloadend = function(e) {
                        console.log('I am here.');
                        var stringvalue=fileReader.result;
                        attachment = stringvalue; //Base 64 encode the file before sending it
                        positionIndex = 0;
                        fileSize =attachment.length;
                        var self = this;
                        var action=component.get("c.readFile");
                        action.setParams({attachment : attachment});
                        action.setCallback(this, function(response) {
                            debugger;
                            var result = response.getReturnValue();
                            const data1 = [];            
                            for(var i=0;i<result.length;i++){
                                var data = result[i];
                                var listOfValues = data.data;
                                var fieldName = data.fieldName;
                                var sfdcFieldName= data.SalesforceFieldName;
                                
                                if(sfdcFieldName ==='UNMAPPED'){
                                    data1.push({SalesforceFieldName : sfdcFieldName, fieldName : fieldName, UnmappedCSS : 'unmappedCSS', actionLabel : 'MAP',data:listOfValues,idOfColoumn:i});
                                }else{
                                    data1.push({SalesforceFieldName : sfdcFieldName, fieldName : fieldName, actionLabel : 'MAP',data:listOfValues,idOfColoumn:i});
                                }
                            }
                            component.set("v.columnData",data1);
                            component.set("v.hadselected", true);
                            component.set("v.uploadCase", false);
                            component.set("v.uploadCase", GroupId);
                            var lineGroupColName = component.get("v.lineGroupColName");
                            if(lineGroupColName == undefined){
                                component.set("v.GroupId",'');
                            }else{
                                component.set("v.GroupId",'Note : Please MAP ' +lineGroupColName+' to create qoute line group');    
                            }
                        });
                        $A.enqueueAction(action);
                        component.set("v.result", false);
                        component.find("file").getElement().value='';//to de-select the file
                    }
                    fileReader.readAsBinaryString(file);
                }
            }else{
                component.set("v.chooseFile", 'Please choose file as CSV format');   
            }
        }else{
            component.set("v.chooseFile", 'Please choose file');
        }
    },
    handleMapSalesforceFields: function(component, event) {
        //call apex class method
        debugger;
        var importlabel = component.get("v.importColumn");
        var record = component.get("v.columnData"); 
        var mapOfFieldNameVsRecord = new Map();
        var ExistingsfdcFieldName =[];
        var sfdcFieldNameList = [];
        for(var i = 0;i< record.length;i++){
            var data = record[i];
            var listOfValues = data.data;
            var sfdcFieldName= data.SalesforceFieldName
            ExistingsfdcFieldName.push(sfdcFieldName);
            var sfdcFieldNameList=JSON.stringify(ExistingsfdcFieldName);
        }
        var action = component.get('c.getSfdcField');
        action.setParams({"sfdcFieldName" : sfdcFieldNameList,
                          "importFormatName":importlabel});
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var custs=[];
                var documentObj = response.getReturnValue();
                for(var i = 0; i < documentObj.length; i++){
                    custs.push(documentObj[i].Name);    
                }
                component.set("v.ListOfFields",custs);
                component.set("v.showFieldPopoUp", true);
            }
        });
        $A.enqueueAction(action);
    },
    handleSelectField: function(component, event,helper) {
        //call apex class method
        /* 
        */
        debugger;
        var idx = event.target.getAttribute('data-index');
        var sfdcFieldName = component.get("v.ListOfFields")[idx];
        var indexOfList = component.get("v.indexOfList");
        var actualRecord = component.get("v.columnData");
        var modifiedData = actualRecord[indexOfList];
        modifiedData.SalesforceFieldName = sfdcFieldName;
        actualRecord[indexOfList] = modifiedData;
        const data1 = [];            
        for(var i=0;i<actualRecord.length;i++){
            var data = actualRecord[i];
            var listOfValues = data.data;
            var fieldName = data.fieldName;
            var sfdcFieldName= data.SalesforceFieldName;
            if(sfdcFieldName ==='UNMAPPED'){
                data1.push({SalesforceFieldName : sfdcFieldName, fieldName : fieldName, UnmappedCSS : 'unmappedCSS', actionLabel : 'MAP',data:listOfValues,idOfColoumn:i});
            }else{
                data1.push({SalesforceFieldName : sfdcFieldName, fieldName : fieldName, actionLabel : 'MAP',data:listOfValues,idOfColoumn:i});
            }
        }
        component.set("v.columnData",data1);
        component.set("v.hadselected", true);
        component.set("v.showFieldPopoUp", false);
    },
    getFieldName : function(component, event) {
        //call apex class method
        debugger;
        var action = component.get('c.getImportFileFormats');
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            var custs=[];
            if (state === "SUCCESS") {
                debugger;
                var documentObj = response.getReturnValue();
                for(var i = 0; i < documentObj.length; i++){
                    custs.push(documentObj[i].SBQQ__ImportFormatName__c);
                }
                component.set("v.ListOfImportFormats",custs);
            }
        });
        $A.enqueueAction(action);
    },
    getMandatoryFields : function(component, event) {
        //call apex class method
        debugger;
        var action = component.get('c.getMandatoryFieldsOfImportColumns');
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            var custs=[];
            if (state === "SUCCESS") {
                debugger;
                var nameVsMandatoryFieldMap = new Map();
                var importDataList = response.getReturnValue();
                if(importDataList != null){
                    for(var i = 0 ;i<importDataList.length ; i++ ){
                        if(importDataList[i].IsCheck__c){
                            var nameOfColoumn = importDataList[i].SBQQ__ImportFormat__r.SBQQ__ImportFormatName__c;
                            if(nameVsMandatoryFieldMap.has(nameOfColoumn)){
                                var existingList = nameVsMandatoryFieldMap.get(nameOfColoumn);
                                existingList.push(importDataList[i].Name);
                                nameVsMandatoryFieldMap.set(nameOfColoumn,existingList);
                            }else{
                                var newList = [];
                                newList.push(importDataList[i].Name);
                                nameVsMandatoryFieldMap.set(nameOfColoumn,newList);
                            }
                        }
                    }
                    component.set("v.remainingFieldtoMap", nameVsMandatoryFieldMap);
                    var keyOfMap = nameVsMandatoryFieldMap.keys();   
                }  
            }
        });
        $A.enqueueAction(action);
    },
    showRowDetails : function(row,cmp,event,helper) {
        // eslint-disable-next-line no-alert
        debugger;
        cmp.set("v.indexOfList" ,row.idOfColoumn);
        helper.handleMapSalesforceFields(cmp, event);
        // alert("Showing opportunity " + row.idOfColoumn+ " closing on " + row.SalesforceFieldName);
    },
    onRecordSizeChangeHelper: function(component, event) {
        //call apex class method
        debugger;
        var importCol='';
        var recordToDisply = component.find("recordSize").get("v.value");
        var action = component.get('c.getSelectImportFormatRecord');
        action.setParams({"selectedImportFormat" : recordToDisply});
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var custs=[];
                var documentObj = response.getReturnValue();
                for(var i = 0; i < documentObj.length; i++){
                    custs.push(documentObj[i].Name);    
                }
                component.set("v.lineGroupImportColName",recordToDisply);
                component.set("v.importColumnName",""+custs);
                component.set("v.uploadCase", true);
                component.set("v.importColumn",recordToDisply);
            }
        });
        $A.enqueueAction(action);
    },  
    onRecordChangeHelper: function(component, event) {
        //call apex class method
        debugger;
        var recordToDisply = component.find("recordSize").get("v.value");
        var action = component.get('c.getRecordForLineGroup');
        action.setParams({"selectedImportFormat" : recordToDisply});
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var custs=[];
                var documentObj = response.getReturnValue();
                for(var i = 0; i < documentObj.length; i++){
                    custs.push(documentObj[i].Name);    
                }
                component.set("v.LineGroup",custs);
                component.set("v.uploadCase", true);
                component.set("v.importColumn",recordToDisply);
            }
        });
        $A.enqueueAction(action);
    },
    createRecordHelper: function(cmp, event) {
        var errorMsg ;
        var isValid = true;
        var messageToDisplay = '';
        var record = cmp.get("v.columnData");
        var recordId = cmp.get("v.recordId"); 
        var mapOfFieldNameVsRecord = new Map();
        var importFormatName = cmp.get("v.importColumn");
        var filedName='';
        var nameVsmandatortyFiledMap = cmp.get('v.remainingFieldtoMap');
        var unMappedColoumn = [];
        var mandatoryFields=[];
        var lineGroupCol = cmp.get("v.lineGroupColName");
        var recordToDisply = cmp.get("v.lineGroupImportColName");
        //var lineGroupCol = cmp.find("lineGroupCol").get("v.value");
        for(var i = 0;i< record.length;i++){
            var data = record[i];
            var listOfValues = data.data;
            var sfdcFieldName= data.SalesforceFieldName;
            if(data.SalesforceFieldName != 'UNMAPPED'){
                unMappedColoumn.push(data.SalesforceFieldName); 
            }
            if(!mapOfFieldNameVsRecord.has(data.SalesforceFieldName)){
                mapOfFieldNameVsRecord.set(data.SalesforceFieldName, listOfValues);
            }  
        }
        debugger;
        var valuesOfMap = nameVsmandatortyFiledMap.get(importFormatName);
        if(valuesOfMap != null){
            for(var i = 0 ; i < valuesOfMap.length;i++){
                if(!unMappedColoumn.includes(valuesOfMap[i])){
                    errorMsg = 'Map mandatory fields.' ;
                    isValid =false;
                    mandatoryFields.push(valuesOfMap[i]);
                } 
            }
        }
        if((isValid === false)){
            cmp.set("v.errorMsg",'Please Map mandatory fields '+mandatoryFields);
            //cmp.find("unmappId").set("v.errors", [{message:"Please Map CSV FILED with SFDC FILED"}]);
        }else{
            var productCodeList = [];
            var code =[];
            var count=0;
            if(mapOfFieldNameVsRecord.has('Qty') || mapOfFieldNameVsRecord.has('Quantity')){
                code= (mapOfFieldNameVsRecord.get('Qty') || mapOfFieldNameVsRecord.get('Quantity'));
                var QtyJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Prod #') || mapOfFieldNameVsRecord.has('Product Code')){
                code= (mapOfFieldNameVsRecord.get('Prod #') || mapOfFieldNameVsRecord.get('Product Code'));
                var ProdJSON=JSON.stringify(code);
                count=code.length;
            }
            if(mapOfFieldNameVsRecord.has('Description')){
                code= mapOfFieldNameVsRecord.get('Description');
                var DescriptionJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('List Price')){
                code= mapOfFieldNameVsRecord.get('List Price');
                var ListPriceJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Disc. %')){
                code= mapOfFieldNameVsRecord.get('Disc. %');
                var DiscJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Unit Price')){
                code= mapOfFieldNameVsRecord.get('Unit Price');
                var UnitPriceJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Net Price')){
                code= mapOfFieldNameVsRecord.get('Net Price');
                var NetPriceJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Avail')){
                code= mapOfFieldNameVsRecord.get('Avail');
                var AvailJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Product Line')){
                code= mapOfFieldNameVsRecord.get('Product Line');
                var ProductLineJSON=JSON.stringify(code);
            }
            if(mapOfFieldNameVsRecord.has('Product Class') || mapOfFieldNameVsRecord.has('ProductType')){
                code= mapOfFieldNameVsRecord.get('Product Class');
                var ProductClassJSON=JSON.stringify(code);
            }
            var action = cmp.get('c.saveRecords');
            action.setParams({"QtyJSONList" : QtyJSON,
                              "ProdJSONList":ProdJSON,
                              "DescriptionJSONList":DescriptionJSON,
                              "ListPriceJSONList":ListPriceJSON,
                              "DiscJSONList":DiscJSON,
                              "UnitPriceJSONList":UnitPriceJSON,
                              "NetPriceJSONList":NetPriceJSON,
                              "AvailJSONList":AvailJSON,
                              "ProductLineJSONList":ProductLineJSON,
                              "ProductClassJSONList":ProductClassJSON,
                              "recordId":recordId,
                              "createLineGroupCol":lineGroupCol,
                              "importFormatName":recordToDisply,
                              "RecordCount":count});
            action.setCallback(this, function(response){
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    messageToDisplay = 'File uploaded successfully'
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": messageToDisplay
                    });
                    toastEvent.fire();
                    var urlEvent = $A.get("e.force:navigateToURL");
                    //var urls=window.location.href;
                    urlEvent.setParams({ 
                        //  "url": urls+"case/"+newCaseId+"/detail" 
                        "url": window.location.protocol+"//"+window.location.hostname + "/apex/SBQQ__sb" + "?id="+recordId
                        //"url": "/500/"+newCaseId+"/"
                    });
                    urlEvent.fire();
                }else{
                    var errors = response.getError();                       
                    //component.set("v.errorMessage",errors[0].message);
                    //messageToDisplay = 'Required fields are mising'
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": errors[0].message
                    });
                    toastEvent.fire(); 
                }
            });
            $A.enqueueAction(action);
        }     
    },
})