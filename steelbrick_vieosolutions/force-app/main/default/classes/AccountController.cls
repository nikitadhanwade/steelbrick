public class AccountController {
public account acc;
public AccountController(ApexPages.StandardController controller) {
this.acc = (Account)controller.getRecord();
}
    public PageReference Save(){
    upsert acc;
    PageReference pNext = new PageReference('/'+acc.id);
    pNext.setRedirect(true);
    return pNext;
}
}