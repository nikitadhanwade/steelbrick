public with sharing class AccountSalesTriggerHelper {

    public static void updateAccountIdOnAccountSales(List<Account_Sales__c> accSalesList){
        System.debug('*****accSalesList******'+accSalesList);
        if(accSalesList.isEmpty()){
            return;    
        }
        Set<String> customerNumSet = new Set<String>();
        for(Account_Sales__c accSales : accSalesList){
            if(accSales.Customer__c != null){
                customerNumSet.add(accSales.Customer__c);     
            }  
        }
        System.debug('*****customerNumSet******'+customerNumSet);
        Map<String,String> accountMap = new Map<String,String>();
        for(Account account: [SELECT Customer__c,Id FROM Account WHERE Customer__c IN:customerNumSet]){
            if(account.Customer__c != null){
                accountMap.put(account.Customer__c, account.Id); 
            }
        }
        System.debug('*****accountMap******'+accountMap);
        for(Account_Sales__c accSales : accSalesList){
            if(accountMap.containsKey(accSales.Customer__c)){
                if(accSales.Customer__c != null)
                accSales.Account__c = accountMap.get(accSales.Customer__c);
            }else{
                accSales.Account__c.addError('Account is not present');    
            }    
        }
        System.debug('*****accSalesList******'+accSalesList);
    }
}