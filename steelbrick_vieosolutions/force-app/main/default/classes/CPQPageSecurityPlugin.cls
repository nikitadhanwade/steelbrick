global class CPQPageSecurityPlugin implements SBQQ.PageSecurityPlugin2 {
    public static Boolean isFieldEditable(String pageName, Schema.SObjectField field) {
        return null;
    }
    
    public static Boolean isFieldEditable(String pageName, Schema.SObjectField field, SObject record) {
        if ((pageName == 'EditLines') && (record instanceof SBQQ__QuoteLine__c)) {
            SBQQ__QuoteLine__c line = (SBQQ__QuoteLine__c)record;
            if ((line.SBQQ__ProductName__c != 'Early Occupancy') && (field == SBQQ__QuoteLine__c.SBQQ__ListPrice__c)) {
                return true;
            }
        }
        return null;
    }
    
    public static Boolean isFieldVisible(String pageName, Schema.SObjectField field) {
        return null;
    }
    
    public static Boolean isFieldVisible(String pageName, Schema.SObjectField field, SObject record) {
        if ((pageName == 'EditLines') && (record instanceof SBQQ__QuoteLine__c)) {
            SBQQ__QuoteLine__c line = (SBQQ__QuoteLine__c)record;
            if ((line.SBQQ__ProductName__c != 'Early Occupancy') && (field == SBQQ__QuoteLine__c.SBQQ__ListPrice__c)) {
                return false;
            }
        }
        return null;
    }
}