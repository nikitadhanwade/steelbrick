@istest
public class CPQPageSecurityPluginTest {
    
    @istest static void TestIsFieldEditable(){
        Schema.DescribeFieldResult F = Account.Industry.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        boolean result = CPQPageSecurityPlugin.isFieldEditable('testPage', T);
        System.assertEquals(null, result);
    }
    
    @istest static void TestIsFieldEditableNegative(){
        Schema.DescribeFieldResult F = Account.Industry.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        SObject obj = new Account(name='test');
        boolean result = CPQPageSecurityPlugin.isFieldEditable('testPage', T,obj);
        System.assertEquals(null, result);
    }
    
    @istest static void TestIsFieldEditablePositive(){
        Product2 prod = new Product2(name='testProd');
        insert prod;
        Schema.DescribeFieldResult F = SBQQ__QuoteLine__c.SBQQ__ListPrice__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        SObject obj = new SBQQ__QuoteLine__c(SBQQ__Product__c = prod.Id);
        boolean result = CPQPageSecurityPlugin.isFieldEditable('EditLines', T,obj);
        System.assertEquals(true, result);
    }
    
    @istest static void TestIsFieldVisible(){
        Schema.DescribeFieldResult F = Account.Industry.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        boolean result = CPQPageSecurityPlugin.isFieldVisible('testPage', T);
        System.assertEquals(null, result);
    }
    
    @istest static void TestIsFieldVisibleNegative(){
        Schema.DescribeFieldResult F = Account.Industry.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        SObject obj = new Account(name='test');
        boolean result = CPQPageSecurityPlugin.isFieldVisible('testPage', T,obj);
        System.assertEquals(null, result); 
    }
    
    @istest static void TestIsFieldVisiblePositive(){
        Product2 prod = new Product2(name='testProd');
        insert prod;
        Schema.DescribeFieldResult F = SBQQ__QuoteLine__c.SBQQ__ListPrice__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        SObject obj = new SBQQ__QuoteLine__c(SBQQ__Product__c = prod.Id);
        boolean result = CPQPageSecurityPlugin.isFieldVisible('EditLines', T,obj);
        System.assertEquals(false, result); 
    }
}