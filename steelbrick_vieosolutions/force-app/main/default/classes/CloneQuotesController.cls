public class CloneQuotesController {
 
    @AuraEnabled
    public static Quote cloneQuote(String recordId) {
      
        List<Quote> listofQuote = new List<Quote>();
        Quote record = new Quote();
        string objectfullname = 'Quote';
        List<String> options = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Quote');
        System.debug('------------targetType'+targetType);
        Schema.DescribeSObjectResult typedescription = targetType.getDescribe();
        Map<String, schema.Sobjectfield> resultMap = typedescription.Fields.getMap();

        string query = 'SELECT Account.name,Opportunity.name,Contact.name,' + string.join(new List<string>(typedescription.Fields.getMap().keySet()), ',') + ' FROM '+ objectfullname;
        query= query +  ' WHERE Id = \''+ recordId +'\' ';
        
        System.debug('------------query'+query);
        if(query.length()>0){
            record = Database.query(query);
        }
        
       /* Schema.DescribeFieldResult fieldResult = Quote.status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        System.debug('----options-----'+options);*/
        
        
        
        return record; 
    } 
    @AuraEnabled
    public static String saveData(List<Quote> quote,String SourceQuotec,String ExpirationDate){ 
        System.debug('--------SourceQuotec------'+SourceQuotec);
        System.debug('--------ExpirationDate------'+ExpirationDate);
        List<Quote> quoteListToUpdate = new List<Quote>();
        Map<Id,Quote> quoteMap = new Map<Id,Quote>();
        String temp = '';
        List<Database.SaveResult> failedAccList = new List<Database.SaveResult>();
        
        system.debug('quote------------'+quote); 
      
        
        if(quote.size()>0){
            for(Quote q:quote){ 
                Quote quotes = new Quote();
                quotes = q.clone(false, false, false, false);
                quotes.Status='Est';
                quotes.put('Source_Quote__c',SourceQuotec);
                if(!string.isEmpty(ExpirationDate)){
                    Date SubscriptionDateMatch = Date.valueOf(ExpirationDate);   
                    quotes.put('ExpirationDate',SubscriptionDateMatch);
                }else{
                   Date nullDate = null;
                    quotes.put('ExpirationDate',nullDate);
                }
                quoteListToUpdate.add(quotes);
                quoteMap.put(q.Id,q);
            }
        }
        system.debug('quoteMap----------'+quoteMap);
        
        //Insert quoteListToUpdate;
        if(quoteListToUpdate.size()>0){
            Database.SaveResult[] quoteListToUpdateresults  = Database.insert(quoteListToUpdate);
            
           System.debug('***quoteListToUpdateresults ****'+quoteListToUpdateresults );
        }
        system.debug('quoteListToUpdate----------'+quoteListToUpdate);
        
        string QuoteLineItem = 'QuoteLineItem';
        Schema.SObjectType QuoteLineItemtargetType = Schema.getGlobalDescribe().get('QuoteLineItem');
        System.debug('------------QuoteLineItemtargetType'+QuoteLineItemtargetType);
        Schema.DescribeSObjectResult QuoteLineItemtargetTypetypedescription = QuoteLineItemtargetType.getDescribe();
        Map<String, schema.Sobjectfield> resultMap = QuoteLineItemtargetTypetypedescription.Fields.getMap();
        
        Set<Id> taskIds=quoteMap.keySet();
        temp=''+taskIds;
        temp=temp.substring(1, 19);
        System.debug('------------temp'+temp);
        
        string QuoteLineItemquery = 'SELECT ' + string.join(new List<string>(QuoteLineItemtargetTypetypedescription.Fields.getMap().keySet()), ',') + ' FROM '+ QuoteLineItem;
        QuoteLineItemquery= QuoteLineItemquery +  ' WHERE QuoteId = \''+ temp +'\' ';
        System.debug('------------QuoteLineItemquery'+QuoteLineItemquery);
        List<QuoteLineItem> cts = Database.query(QuoteLineItemquery);
         List<QuoteLineItem> QuoteLineListToUpdate = new  List<QuoteLineItem> ();
        if(cts.size()>0){
            for(QuoteLineItem qli : cts){
                QuoteLineItem QuoteLI = qli.clone(false, false, false, false);
                if(quoteMap.containsKey(qli.QuoteId)){
                    QuoteLI.QuoteId=quoteListToUpdate[0].id;
                    QuoteLineListToUpdate.add(QuoteLI);
                }
            }
        }
        if(QuoteLineListToUpdate.size()>0){
            
            Database.SaveResult[] QuoteLineListToUpdateresults  = Database.insert(QuoteLineListToUpdate);
            System.debug('***QuoteLineListToUpdateresults ****'+QuoteLineListToUpdateresults );
        }
        System.debug('------------cts'+cts);
                 
        return quoteListToUpdate[0].id;
    }
}