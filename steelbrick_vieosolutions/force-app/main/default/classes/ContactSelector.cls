public with sharing class ContactSelector {

    /* This method is used to get preferences of contact associated with respective campaign member 
who's category = communication sub category = call card and opetion - Never
User Story - E2
11-1-2018 */
    
    public static List<Contact> getContacts(String payeeId){
        List<Contact> preferenceList = new List<Contact>([SELECT AccountId,Description,Email,LastName,Name FROM Contact]);
        if(preferenceList.size()>0){
            return preferenceList;
        }
        else{
            return null;
        }
    }
    
}