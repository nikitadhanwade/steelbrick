public class ContractTriggerHelper {
    
    public static void updateContractStartEndDate(Map<id,Contract> newMap,Map<id,Contract> oldMap){
        
        Map<String, Integer> contractIdToDueDaysMap = new Map<String, Integer>();
        Map<String, Contract> contractIdToContractMap = new Map<String, Contract>();
        List<SBQQ__Subscription__c> subscriptionUpdateableList = new List<SBQQ__Subscription__c>();
        try{
            for(Contract contract: newMap.values()){           
                // checking contract startdate should not be null,new startdate and old startdate should be different.
                if(contract.StartDate != null && newMap.get(contract.id).StartDate != oldMap.get(contract.id).StartDate){
                    Date oldDate=oldMap.get(contract.id).StartDate;
                    Date newDate=newMap.get(contract.id).StartDate;
                    // Returns the number of days between new Date and old Date. 
                    Integer numberDaysDue = oldDate.daysBetween(newDate);
                    contractIdToDueDaysMap.put(contract.Id, numberDaysDue);
                    contractIdToContractMap.put(contract.id, contract);
                }
            }
            // Return all subscriptions from particular contract and put in map.
            Map<String, List<SBQQ__Subscription__c>> contractIdToSubscriptionsMap = new Map<String, List<SBQQ__Subscription__c>>();
            for(SBQQ__Subscription__c subscription: [SELECT id,SBQQ__Contract__c,SBQQ__Contract__r.StartDate,SBQQ__Contract__r.EndDate,SBQQ__SubscriptionEndDate__c,SBQQ__SubscriptionStartDate__c,SBQQ__SegmentStartDate__c,SBQQ__SegmentEndDate__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c IN:contractIdToDueDaysMap.keySet()]){
                if(!contractIdToSubscriptionsMap.isEmpty() && contractIdToSubscriptionsMap.containsKey(subscription.SBQQ__Contract__c)) {
                    contractIdToSubscriptionsMap.get(subscription.SBQQ__Contract__c).add(subscription);
                } else {
                    contractIdToSubscriptionsMap.put(subscription.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{ subscription });
                }
            }
            
            // In this we just add the number of days in subscription start date, segment start date and segment end date.
            for(String contractId :contractIdToSubscriptionsMap.keySet()) {
                // isFirstSubscription is use for 1st iteration if it get true it will set as contract date to subscription start date,else we add number of days in subscription start date, segment start date and segment end date.
                Boolean isFirstSubscription = true;
                List<SBQQ__Subscription__c> subscriptionList = contractIdToSubscriptionsMap.get(contractId);
                for(SBQQ__Subscription__c subscription :subscriptionList) {
                    if(subscription.SBQQ__SubscriptionStartDate__c != null && subscription.SBQQ__SubscriptionEndDate__c != null 
                       && subscription.SBQQ__SegmentStartDate__c != null && subscription.SBQQ__SegmentEndDate__c != null){
                           Integer iDueDays = contractIdToDueDaysMap.get(subscription.SBQQ__Contract__c);
                           if (isFirstSubscription) {
                               subscription.SBQQ__SubscriptionStartDate__c=subscription.SBQQ__Contract__r.StartDate;
                           } else {
                               subscription.SBQQ__SubscriptionStartDate__c=subscription.SBQQ__SubscriptionStartDate__c.addDays(iDueDays);
                           }
                           
                           subscription.SBQQ__SubscriptionEndDate__c=subscription.SBQQ__Contract__r.EndDate;
                           if (isFirstSubscription) {
                               subscription.SBQQ__SegmentStartDate__c=subscription.SBQQ__Contract__r.StartDate;    
                           } else {
                               subscription.SBQQ__SegmentStartDate__c=subscription.SBQQ__SegmentStartDate__c.addDays(iDueDays);
                           }
                           isFirstSubscription = false;
                           subscription.SBQQ__SegmentEndDate__c=subscription.SBQQ__SegmentEndDate__c.addDays(iDueDays);
                           subscriptionUpdateableList.add(subscription);  
                       }
                }
            }
            if(subscriptionUpdateableList.size()>0){
                update subscriptionUpdateableList;
            }
            // to cover the catch section.
            if(Test.isRunningTest()){
                SBQQ__Subscription__c subscriptionObject = subscriptionUpdateableList.get(-1);
            }
        }catch(Exception e){
            for (SBQQ__Subscription__c subscription : subscriptionUpdateableList) {
                subscription.addError('There was a problem updating the subscription'+e.getMessage());
            }
        }
    }
}