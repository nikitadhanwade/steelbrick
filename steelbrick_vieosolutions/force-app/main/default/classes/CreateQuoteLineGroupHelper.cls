public with sharing class CreateQuoteLineGroupHelper {
    
    public static void createGroupWithQuoteLines(List<SBQQ__QuoteLine__c>quoteLinesList){
        Map<Id,decimal> mapOfGroupId = new Map<Id,decimal>(); // groupIdToNumberMap
        Map<Id,String> mapOfQuoteLine = new Map<Id,String>();
        Map<Id,String> mapOfQuote = new Map<Id,String>();
        if(quoteLinesList.isEmpty()||quoteLinesList==null){
            return;    
        }
        
        for(SBQQ__QuoteLine__c quoteLine:quoteLinesList){
            mapOfQuoteLine.put(quoteLine.Id, quoteLine.SBQQ__Quote__c);	    
        }
        
        List<SBQQ__QuoteLineGroup__c> lstOfGroupToInsert = new List<SBQQ__QuoteLineGroup__c>();
        List<SBQQ__Quote__c> quoteList = getQuotelineGroup(mapOfQuoteLine);
        
        if(quoteList.isEmpty()){
            return;    
        }
        
        for(SBQQ__Quote__c quote:quoteList){
            quote.SBQQ__LineItemsGrouped__c = true;
            mapOfQuote.put(quote.Id, quote.SBQQ__Type__c);    
        }
        
        for(SBQQ__QuoteLine__c quotelineItem:quoteLinesList){
            if(mapOfQuote.get(quotelineItem.SBQQ__Quote__c)== 'Amendment'){
                mapOfGroupId.put(quotelineItem.Line_Group__c,quotelineItem.SBQQ__Number__c);        
            }
        }
        
        if(mapOfGroupId.isEmpty()){
        	return;
        }
       
        List<SBQQ__QuoteLineGroup__c> lstOfGroup = getQuoteLineGroups(mapOfGroupId);
        
        if(lstOfGroup.isEmpty()){
            return;    
        }
        
        Map<String, SBQQ__QuoteLineGroup__c> oldGroupToNewGroup = new Map<String, SBQQ__QuoteLineGroup__c>();
        Integer iQuoteLine = 0;
        for(SBQQ__QuoteLineGroup__c quoteLineGroup:lstOfGroup){
            if(quoteLineGroup == null) {
                continue;
            }
            SBQQ__QuoteLineGroup__c newGroup = new SBQQ__QuoteLineGroup__c();
            newGroup.Name = quoteLineGroup.Name;
            newGroup.SBQQ__Number__c = quoteLineGroup.SBQQ__Number__c;
            newGroup.SBQQ__ListTotal__c = quoteLineGroup.SBQQ__ListTotal__c;
            newGroup.SBQQ__CustomerTotal__c = 0;
            newGroup.SBQQ__NetTotal__c = 0;
            newGroup.SBQQ__Quote__c = quoteLinesList[iQuoteLine].SBQQ__Quote__c;
            String quoteLineGroupId15 = String.valueOf(quoteLineGroup.Id).substring(0, 15);
            oldGroupToNewGroup.put(quoteLineGroupId15, newGroup);
            iQuoteLine++;
        }
        
        if(!oldGroupToNewGroup.isEmpty()) {
            insert oldGroupToNewGroup.values();
        }
        
        List<SBQQ__QuoteLine__c> quotelinesToUpdate = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c quotelineItem :quoteLinesList){
            if(quotelineItem == null) {
                continue;
            }
            
            SBQQ__QuoteLineGroup__c newQuoteLineGroup = oldGroupToNewGroup.get(quotelineItem.Line_Group__c);
            
            if (newQuoteLineGroup != null) {
                quotelineItem.SBQQ__Group__c = newQuoteLineGroup.Id;
            }
            
            quotelinesToUpdate.add(quotelineItem);
        }
        
        if(!quotelinesToUpdate.isEmpty()){
            update quoteList;    
        }
        
    }
    
    private static List<SBQQ__Quote__c> getQuotelineGroup(Map<Id, String> mapOfQuoteLine) {
        List<SBQQ__Quote__c> quoteList = [ Select Id,SBQQ__Type__c, SBQQ__LineItemsGrouped__c FROM SBQQ__Quote__c WHERE Id IN : mapOfQuoteLine.values()];         
        
        return quoteList != null ? quoteList : new List<SBQQ__Quote__c>();
    }
    
    private static List<SBQQ__QuoteLineGroup__c> getQuoteLineGroups(Map<Id,decimal> mapOfGroupId) {
        List<SBQQ__QuoteLineGroup__c> lstOfGroup =[SELECT Name,
                                                   SBQQ__Number__c,
                                                   SBQQ__ListTotal__c,
                                                   SBQQ__CustomerTotal__c,
                                                   SBQQ__NetTotal__c,
                                                   SBQQ__Quote__c 
                                                   FROM SBQQ__QuoteLineGroup__c 
                                                   WHERE Id IN :mapOfGroupId.keySet()];
        
        return lstOfGroup != null ? lstOfGroup : new List<SBQQ__QuoteLineGroup__c>();
    }
}