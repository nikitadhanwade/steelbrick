global class LeadPricingBatch implements Database.Batchable<sObject> {
    
    String query;
    List<product2> residentProductData = new List<product2>();
    List<product2> residentProductList = new List<product2>();
    Map<Integer,Decimal> monthDiscountMap = new Map<Integer,Decimal>();
    Set<Integer> setStrings = new Set<Integer>();
    Set<Integer> keyValues = new Set<Integer>();
    
    String ResidentLabel=Label.Resident;
    String Listprice;
    
    global LeadPricingBatch() {
        this.query = 'select name,Category__c,Family,Vieo__c,(SELECT UnitPrice FROM PricebookEntries) from product2 where IsActive = true';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<product2> ProductList) {
        String Query;
        List<String> VieoIdsList = new List<String>();
        List<sObject> InsertLeadPrice = new List<sObject>();
        List<Term_Discount__c> TermDiscountList = new List<Term_Discount__c>();
        Map<String,Term_Discount__c> TermDiscountMap = new Map<String,Term_Discount__c>();
        List<String> CategoryList = new List<String>();
        Map<String,Map<String,product2>> DedicatedOfficeMap = new Map<String,Map<String,product2>>();   
        
        // Deleting Existing record of Lead price objects.
        List<Lead_Price__c> leadPriceData= [SELECT Id,Family__c,Lead_Price__c,Membership__c,Category__c,PriceBook_Price__c,Vieo__c FROM Lead_Price__c];
        List<Database.DeleteResult> failedAccList = new List<Database.DeleteResult>();
        Database.DeleteResult[] drList = database.delete(leadPriceData);
        
        // Getting all Family to put in select query to make dynamic.
        List<aggregateResult> FamilyList= [SELECT Family FROM Product2 where Family != null group by Family];
        for(aggregateResult pro :FamilyList){
            CategoryList.add(''+pro.get('Family'));
            
        }
        
        // For test coverage purpose.
        if(Test.isRunningTest()){
            Query ='select name,Category__c,Family,Vieo__c,(SELECT Pricebook2Id,UnitPrice,UseStandardPrice FROM PricebookEntries)  from product2 where IsActive = true and Category__c =: ResidentLabel and Family IN: CategoryList';
        }else{
            Query='select name,Category__c,Family,Vieo__c,(SELECT Pricebook2Id,UnitPrice,UseStandardPrice FROM PricebookEntries where Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard = false))  from product2 where IsActive = true and Category__c =: ResidentLabel and Family IN: CategoryList';
        }
        
        list<product2> records = Database.query(Query);
        for(product2 productData : records){ 
            VieoIdsList.add(productData.Vieo__c);
        }
        
        if(VieoIdsList.size() > 0){
            TermDiscountList=[SELECT Name,Markup_Discount__c,Term__c,Lookup_Price__c,vieo__c  FROM Term_Discount__c where Term__c=:24 and vieo__c In :VieoIdsList];   
            for(Term_Discount__c TermDiscount : TermDiscountList){
                TermDiscountMap.put(TermDiscount.Vieo__c,TermDiscount);
            }   
        }
        
        Map<Decimal,Decimal> discountMap = new Map<Decimal,Decimal>();
        if(TermDiscountList .size() > 0) {
            for(product2 productData : records){
                for(Term_Discount__c TermDiscount : TermDiscountList){
                    if(TermDiscountMap.containsKey(productData.Vieo__c)){
                        // Taking Discount from term discount object for particular product.
                        if(TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c>0){
                            Decimal discount=TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c/100;
                            discount=1+discount;
                            discountMap.put(TermDiscountMap.get(productData.Vieo__c).Term__c,discount);
                        }else if(TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c<0){
                            discountMap.put(TermDiscountMap.get(productData.Vieo__c).Term__c,TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c);
                        }else{
                            Decimal discount=TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c/100;
                            discountMap.put(TermDiscountMap.get(productData.Vieo__c).Term__c,discount);
                        }
                    }
                    productData.put('SBQQ__CompoundDiscountRate__c',TermDiscountMap.get(productData.Vieo__c).Markup_Discount__c);
                    
                }
                residentProductList=getNewProducts(productData,discountMap);
            }
        }
        
        if(residentProductList.size()>0){
            for(product2 DedicatedOfficeProduct : residentProductList){
                //already exist in Map then get the inner map and check lead price .
                //If existing Map's lead price is greater then DedicatedOfficeProduct then replace with DedicatedOfficeProduct 
                if(DedicatedOfficeProduct.Vieo__c != null && DedicatedOfficeMap.containsKey(DedicatedOfficeProduct.Vieo__c)){
                    Map<String,product2> familyVsproduct = DedicatedOfficeMap.get(DedicatedOfficeProduct.Vieo__c);
                    if(DedicatedOfficeProduct.Family != null) {
                        if(familyVsproduct.containsKey(DedicatedOfficeProduct.Family)) { 
                            if(Integer.valueOf(familyVsproduct.get(DedicatedOfficeProduct.Family).Lead_Price__c) > Integer.valueOf(DedicatedOfficeProduct.Lead_Price__c)) {  
                                familyVsproduct.put(DedicatedOfficeProduct.Family,DedicatedOfficeProduct);
                            }
                        }else{
                            familyVsproduct.put(DedicatedOfficeProduct.Family,DedicatedOfficeProduct);
                        }
                        DedicatedOfficeMap.put(DedicatedOfficeProduct.Vieo__c,familyVsproduct);
                    }     
                }else{
                    Map<String,product2> familyVsproduct = new Map<String,product2>();
                    familyVsproduct.put(DedicatedOfficeProduct.Family,DedicatedOfficeProduct);
                    DedicatedOfficeMap.put(DedicatedOfficeProduct.Vieo__c,familyVsproduct);
                    
                }
            } 
            
            // Insert Resident records in lead price object
            for(string VieoId:DedicatedOfficeMap.keyset()){
                
                Map<String,product2> familyVsproduct = DedicatedOfficeMap.get(VieoId);
                for(string FamilyName:familyVsproduct.keyset()){
                    Lead_Price__c LeadPrice = new Lead_Price__c();
                    LeadPrice.Membership__c =familyVsproduct.get(FamilyName).Name;
                    LeadPrice.Family__c = familyVsproduct.get(FamilyName).Family;
                    LeadPrice.Lead_Price__c = decimal.valueOf(familyVsproduct.get(FamilyName).Lead_Price__c);
                    LeadPrice.PriceBook_Price__c = decimal.valueOf(familyVsproduct.get(FamilyName).PriceBook_Price__c);
                    LeadPrice.Category__c = familyVsproduct.get(FamilyName).Category__c;
                    LeadPrice.Vieo__c = familyVsproduct.get(FamilyName).Vieo__c;
                    LeadPrice.X24M_Term_Discount__c = familyVsproduct.get(FamilyName).SBQQ__CompoundDiscountRate__c;
                    InsertLeadPrice.add(LeadPrice);
                    
                    
                }
            }
            
            // Insert Non Resident records in lead price object
            list<product2> NonResidentProductList = [select name,Category__c,Family,Vieo__c,(SELECT UnitPrice FROM PricebookEntries)  from product2 where IsActive = true AND Category__c =: label.Non_Resident];
            for(product2 NonResidentProductData : NonResidentProductList){
                Lead_Price__c LeadPrice = new Lead_Price__c();
                LeadPrice.Membership__c =NonResidentProductData.Name;
                LeadPrice.Family__c = NonResidentProductData.Family;
                LeadPrice.Category__c = NonResidentProductData.Category__c;
                LeadPrice.Vieo__c = NonResidentProductData.Vieo__c;
                for(PricebookEntry PriceBookProductData : NonResidentProductData.PricebookEntries){
                    LeadPrice.Lead_Price__c = PriceBookProductData.UnitPrice;
                    LeadPrice.PriceBook_Price__c = PriceBookProductData.UnitPrice;
                }
                InsertLeadPrice.add(LeadPrice);
            }
            // Insert Additional product in lead price object
            list<product2> AdditionalProductList = [select name,Category__c,Family,Vieo__c,(SELECT UnitPrice FROM PricebookEntries)  from product2 where IsActive = true AND Category__c =: label.AdditionalServices];
            for(product2 AdditionalProductData : AdditionalProductList){
                Lead_Price__c LeadPrice = new Lead_Price__c();
                LeadPrice.Membership__c =AdditionalProductData.Name;
                LeadPrice.Category__c = AdditionalProductData.Category__c;
                LeadPrice.Vieo__c = AdditionalProductData.Vieo__c;
                LeadPrice.Family__c = AdditionalProductData.Family;
                for(PricebookEntry PriceBookProductData : AdditionalProductData.PricebookEntries){
                    LeadPrice.Lead_Price__c = PriceBookProductData.UnitPrice;
                    LeadPrice.PriceBook_Price__c = PriceBookProductData.UnitPrice;
                }
                InsertLeadPrice.add(LeadPrice);
            }
        }
        if(InsertLeadPrice.size()>0){
            insert InsertLeadPrice;
        }
    } 
    
    public List<product2> getNewProducts(product2 pro,Map<Decimal,Decimal> discountMap){
        decimal months;
        if(discountMap.size()>0){
            for(Decimal discount : discountMap.keySet()){
                // Calculating Pricebook price for particular discount.
                if(pro.PricebookEntries.size() > 0 && pro.PricebookEntries[0]!=null){
                    
                    monthDiscountMap =monthDiscountMap == null ? new Map<Integer,Decimal>() : monthDiscountMap;
                    Integer disc = (Integer)discountMap.get(discount);
                    if(disc == 0) {
                        months = Math.round(pro.PricebookEntries[0].UnitPrice);
                    }else if(disc<0){
                        
                        String surcharge= String.valueOf(discountMap.get(discount)/100);
                        Decimal Caldiscount = Decimal.valueOf(surcharge.substring(1));
                        months = Math.round(((pro.PricebookEntries[0].UnitPrice)-(pro.PricebookEntries[0].UnitPrice*Caldiscount)));
                    } else {
                        months = Math.round((pro.PricebookEntries[0].UnitPrice*discountMap.get(discount)));
                    }
                    months = roundUpCalculate(months);
                    pro.put('Lead_Price__c',String.valueOf(months));
                    pro.put('PriceBook_Price__c',String.valueOf(pro.PricebookEntries[0].UnitPrice));
                    residentProductData.add(pro);
                }
            }
        }
        return residentProductData;
    }
    public static decimal roundUpCalculate(decimal price){
        // Rounding decimal value (i.e 145 to 150).
        String lastvaluesss= String.valueOf(price).right(1);
        
        if(lastvaluesss.equals('1')){
            price=Integer.valueOf(price)+9;
        }else if(lastvaluesss.equals('2')){
            price=Integer.valueOf(price)+8;
        }else if(lastvaluesss.equals('3')){
            price=Integer.valueOf(price)+7;
        }else if(lastvaluesss.equals('4')){
            price=Integer.valueOf(price)+6;
        }else if(lastvaluesss.equals('5')){
            price=Integer.valueOf(price)+5;
        }else if(lastvaluesss.equals('6')){
            price=Integer.valueOf(price)+4;
        }else if(lastvaluesss.equals('7')){
            price=Integer.valueOf(price)+3;
        }else if(lastvaluesss.equals('8')){
            price=Integer.valueOf(price)+2;
        }else if(lastvaluesss.equals('9')){
            price=Integer.valueOf(price)+1;
        }else{
            price=price;
        }
        return price;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}