@istest
public class LeadPricingBatchTest {

    @isTest static void testLeadPricingBatch(){
        test.startTest();
        
        Pricebook2 pricebook = new Pricebook2(name='Atlanta Price book',IsActive=true);
        insert pricebook;
        
         
        List<Pricebook2> lstopp =[select Name,IsStandard from Pricebook2 where id=: pricebook.id];
        System.debug('lstopp------'+lstopp);
        system.assertEquals(1,lstopp.size());
        
        Vieo__c vieo = new Vieo__c(Name='Atlanta',Price_Book__c = pricebook.Id);
        insert vieo;
        Vieo__c vieo1 = new Vieo__c(Name='Dalla',Price_Book__c = pricebook.Id);
        insert vieo1;
        Vieo__c vieo2 = new Vieo__c(Name='California',Price_Book__c = pricebook.Id);
        insert vieo2;
        
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('pricebookId=-----'+pricebookId);
        Product2 pro = new Product2(Name = 'Test 1',Category__c = 'Resident Memberships',ProductCode = '100',Description = 'For testing 1',Capacity__c = '5',SQ_FT__c = 250,Location__c = 'window',Vieo__c = vieo.Id,IsActive = true,Family = 'Dedicated Office');
        insert pro;
      
        Product2 pro1 = new Product2(Name = 'Test 3',Category__c = 'Resident Memberships',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo1.Id,IsActive = true,Family = 'Team Room');
        insert pro1;
        
        Product2 pro2 = new Product2(Name = 'Test 4',Category__c = 'Resident Memberships',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo2.Id,IsActive = true,Family = 'Dedicated Desk');
        insert pro2;
        
        Product2 pro3 = new Product2(Name = 'Test 5',Category__c = 'Resident Memberships',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo1.Id,IsActive = true,Family = 'Dedicated Office');
        insert pro3;
        
        Product2 pro4 = new Product2(Name = 'Test 6',Category__c = 'Resident Memberships',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo1.Id,IsActive = true,Family = 'Team Room');
        insert pro4;
        
        Product2 pro5 = new Product2(Name = 'Test 7',Category__c = 'Non Resident Memberships',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo2.Id,IsActive = true,Family = 'Team Room');
        insert pro5;
        
        Product2 pro6 = new Product2(Name = 'Test 8',Category__c = 'Additional Services',ProductCode = '200',Description = 'For testing 2',Capacity__c = '10',SQ_FT__c = 350,Location__c = 'window',Vieo__c = vieo.Id,IsActive = true,Family = 'Team Room');
        insert pro6;
        Product2 pro7 = new Product2(Name = 'Test 9',Category__c = 'Resident Memberships',ProductCode = '100',Description = 'For testing 1',Capacity__c = '5',SQ_FT__c = 250,Location__c = 'window',Vieo__c = vieo.Id,IsActive = true,Family = 'Dedicated Office');
        insert pro7;
        Product2 pro8 = new Product2(Name = 'Test 10',Category__c = 'Resident Memberships',ProductCode = '100',Description = 'For testing 1',Capacity__c = '5',SQ_FT__c = 250,Location__c = 'window',Vieo__c = vieo.Id,IsActive = true,Family = 'Dedicated Office');
        insert pro8;
        Product2 pro9 = new Product2(Name = 'Test 10',Category__c = 'Resident Memberships',ProductCode = '100',Description = 'For testing 1',Capacity__c = '5',SQ_FT__c = 250,Location__c = 'window',Vieo__c = vieo.Id,IsActive = true,Family = 'Dedicated Office');
        insert pro9;
        List<Product2> lstopp1 =[select Name from Product2 where id=: pro6.id];
        system.assertEquals(1,lstopp1.size());
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro.Id,UnitPrice = 3640, IsActive = true,UseStandardPrice=false);
        insert standardPrice;
        
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro1.Id,UnitPrice = 2590, IsActive = true,UseStandardPrice=false);
        insert standardPrice1;
        
        PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro2.Id,UnitPrice = 870, IsActive = true,UseStandardPrice=false);
        insert standardPrice2;
        
        PricebookEntry standardPrice3 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro3.Id,UnitPrice = 520, IsActive = true,UseStandardPrice=false);
        insert standardPrice3;
        
        PricebookEntry standardPrice4 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro4.Id,UnitPrice = 1800, IsActive = true,UseStandardPrice=false);
        insert standardPrice4;
        
        PricebookEntry standardPrice5 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro5.Id,UnitPrice = 1400, IsActive = true,UseStandardPrice=false);
        insert standardPrice5;
        
        PricebookEntry standardPrice6 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro6.Id,UnitPrice = 0, IsActive = true,UseStandardPrice=false);
        insert standardPrice6;
        PricebookEntry standardPrice7 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro7.Id,UnitPrice = 2590, IsActive = true,UseStandardPrice=false);
        insert standardPrice7;
        PricebookEntry standardPrice8 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro8.Id,UnitPrice = 260, IsActive = true,UseStandardPrice=false);
        insert standardPrice8;
        PricebookEntry standardPrice9 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = pro9.Id,UnitPrice = 1564, IsActive = true,UseStandardPrice=false);
        insert standardPrice9;
        List<PricebookEntry> lstopp2 =[select UnitPrice from PricebookEntry where id=: standardPrice6.id];
        system.assertEquals(1,lstopp2.size());
        
        List <Lead_Price__c> LeadPrice = new List<Lead_Price__c>();
        for(integer i = 0; i<200; i++){
            Lead_Price__c LeadPriceRecord = new Lead_Price__c(Family__c='Dedicated Desk',Lead_Price__c=100.00,Membership__c='Dedicated',Category__c='Resident Memberships',PriceBook_Price__c=254.00,Vieo__c=vieo.id); 
            LeadPrice.add(LeadPriceRecord);
        }
        insert LeadPrice;
         
        
        Term_Discount__c termdiscount = new Term_Discount__c(Name='1 Month',Markup_Discount__c=-10,Term__c=24,Lookup_Price__c=true,vieo__c=vieo.Id);
        insert termdiscount;
        Term_Discount__c termdiscount1 = new Term_Discount__c(Name='2 Month',Markup_Discount__c=0,Term__c=24,Lookup_Price__c=true,vieo__c=vieo1.Id);
        insert termdiscount1;
         Term_Discount__c termdiscount2 = new Term_Discount__c(Name='3 Month',Markup_Discount__c=10,Term__c=24,Lookup_Price__c=true,vieo__c=vieo2.Id);
        insert termdiscount2;
     
        
        List<Term_Discount__c> lstopp3 =[select Name from Term_Discount__c where id=: termdiscount1.id];
        system.assertEquals(1,lstopp3.size());
        
        System.schedule('Lead Pricing Scheduler','0 30 23 * * ?', new LeadPricingScheduler());
        //LeadPricingBatch lpb= new LeadPricingBatch();
        //Database.executeBatch(lpb);
        test.stopTest();
        
    }
}