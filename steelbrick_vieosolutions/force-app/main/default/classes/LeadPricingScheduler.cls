global class LeadPricingScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		database.executebatch(new LeadPricingBatch());
	}
    
    /*
        LeadPricingScheduler LeadPricingSchedulerTest = new LeadPricingScheduler();
        String cronexpression = '0 0 0 ? * * *';
        System.schedule('Lead Pricing Scheduler', cronexpression, LeadPricingSchedulerTest);

*/
	
}