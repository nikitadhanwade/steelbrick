public class LightningLookupController {
    @AuraEnabled
    public static List < sObject > fetchAccount(String searchKeyWord, String query,String isSOSL) {
        List < sObject > returnList = new List < sObject >() ;
        if(isSOSL == 'false'){
            returnList = LightningLookupSelector.getSearchData(query);
            return returnList;
        }else{
            List<List<sObject>> returnLists = new List<List<sObject>>();
            returnLists = LightningLookupSelector.getSearch(query);
            if(returnLists <> NULL && returnLists.size() >2){
            	returnList.addAll(returnLists[0]);
                returnList.addAll(returnLists[1]);
                returnList.addAll(returnLists[2]);
            }else if(returnLists <> NULL && returnLists.size() >1){
                returnList.addAll(returnLists[0]);
                returnList.addAll(returnLists[1]);
            }
            else if(returnLists <> NULL && returnLists.size() >0){
                 returnList.addAll(returnLists[0]);
            }
            return returnList;
        }
       // return null;
    }
    
     @AuraEnabled
    public static Boolean getContact(String payeeId){
        Boolean preference= false;
       
            List<Contact> lstPreference= ContactSelector.getContacts(payeeId);
            if(lstPreference <> NULL && lstPreference.size()>0){
                preference=true;
                return preference;
            }
        return preference;
    }
}