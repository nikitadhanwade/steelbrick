global class MyPageSecurityPlugin implements SBQQ.PageSecurityPlugin2 {
    public Boolean isFieldEditable(String pageName, Schema.SObjectField field) {
        System.debug('inside 1==>'+field);
        return null;
    }
    
    public Boolean isFieldEditable(String pageName, Schema.SObjectField field, SObject record) {
        System.debug('inside 2==>'+field);
        return null;
    }
    
    public Boolean isFieldVisible(String pageName, Schema.SObjectField field) {
         System.debug('inside 3==>'+field);
        return null;
    }
    
    public Boolean isFieldVisible(String pageName, Schema.SObjectField field, SObject record) {
         System.debug('inside 4==>'+field);
        //System.debug('field==>'+field);
        //System.debug('record==>'+record);
        if ((pageName == 'EditLines') && (record instanceof SBQQ__QuoteLine__c)) {
            SBQQ__QuoteLine__c line = (SBQQ__QuoteLine__c)record;
            if ((line.SBQQ__Bundled__c == true) && (field != SBQQ__QuoteLine__c.SBQQ__ProductName__c)) {
                return false;
            }
        }
        return null;
    }
}