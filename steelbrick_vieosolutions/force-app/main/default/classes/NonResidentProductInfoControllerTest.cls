@istest
public class NonResidentProductInfoControllerTest {

    @istest static void testGetNonResidentProductList(){
        List<PricebookEntry> resident = new List<PricebookEntry>();
        List<Vieo__c> vieolist = new List<Vieo__c>();
        PriceBook2 pb2=new PriceBook2();
        pb2.Name = 'test';
        pb2.IsActive = true;
        insert pb2;
        
        Vieo__c vieo = new Vieo__c();
        vieo.Name = 'california';
        vieo.Price_Book__c = pb2.id;
        insert vieo;
        
        Id pricebookId = Test.getStandardPricebookId();

        Product2 pro = new Product2();
        pro.Name = 'Test 1';
        pro.Category__c = 'Non Resident Memberships';
        pro.ProductCode = '100';
        pro.Description = 'For testing 1';
        pro.Capacity__c = '5';
        pro.SQ_FT__c = 250;
        pro.Location__c = 'window';
        pro.Vieo__c = vieo.Id;
        insert pro;
        
        Product2 pro1 = new Product2();
        pro1.Name = 'Test 2';
        pro1.Category__c = 'Additional Services';
        pro1.ProductCode = '200';
        pro1.Description = 'For testing 2';
        pro1.Capacity__c = '10';
        pro1.SQ_FT__c = 350;
        pro1.Location__c = 'window';
        pro1.Vieo__c = vieo.Id;
        insert pro1;
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 3640, IsActive = true);
        insert standardPrice;
        
        PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro1.Id,
            UnitPrice = 1800, IsActive = true);
        insert standardPrice1;
        NonResidentProductInformationController NP = new NonResidentProductInformationController();
        NonResidentProductInformationController.currentRecordId=vieo.id;
        resident=NonResidentProductInformationController.getNonResidentProductList();
        resident=NonResidentProductInformationController.getAdditionalServicesList();
        vieolist=NonResidentProductInformationController.getvieoList();
        
    }
}