public class NonResidentProductInformationController {
    
    public static string currentRecordId{get;set;}
    public static list<Integer> dummyList {get;set;}
    
    public NonResidentProductInformationController(){
        dummyList = new list<Integer>();
        dummyList.add(1);
    }
    public static List<PricebookEntry> getNonResidentProductList(){
        Vieo__c objVieo =[SELECT Name,Price_Book__c FROM Vieo__c where id=:currentRecordId];
        //list<product2> productList = [select name,Category__c,ProductCode,Description,Capacity__c,SQ_FT__c,Location__c,(SELECT Pricebook2Id,UnitPrice,UseStandardPrice FROM PricebookEntries where Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard = false))  from product2 where Vieo__c=:currentRecordId AND Category__c =: label.Non_Resident ORDER BY name];
        list<PricebookEntry> pricebookEntryList = [SELECT Product2.ProductCode,Product2.Name,Product2.Description,Product2.Capacity__C,Product2.SQ_FT__c,Product2.Location__c,UnitPrice FROM PricebookEntry where Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard = false) AND pricebook2id =:objVieo.price_book__c AND Product2.Category__c =: Label.Non_Resident AND Product2.Vieo__c=:currentRecordId AND Product2.IsActive = true ORDER BY Product2.name];
        return pricebookEntryList;
    }
    public static List<PricebookEntry> getAdditionalServicesList(){
        Vieo__c objVieo =[SELECT Name,Price_Book__c FROM Vieo__c where id=:currentRecordId];
        //list<product2> productList = [select name,Category__c,ProductCode,Description,Capacity__c,SQ_FT__c,Location__c,(SELECT Pricebook2Id,UnitPrice,UseStandardPrice FROM PricebookEntries where Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard = false))  from product2 where Vieo__c=:currentRecordId AND Category__c =: label.AdditionalServices ORDER BY name];
        list<PricebookEntry> pricebookEntryList = [SELECT Product2.ProductCode,Product2.Name,Product2.Description,Product2.Capacity__C,Product2.SQ_FT__c,Product2.Location__c,UnitPrice FROM PricebookEntry where Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard = false) AND pricebook2id =:objVieo.price_book__c AND Product2.Category__c =: Label.AdditionalServices AND Product2.Vieo__c=:currentRecordId ORDER BY Product2.name];
        return pricebookEntryList;
    }
    
    public static List<Vieo__c> getvieoList(){
        list<Vieo__c> vieoList = [SELECT Name FROM Vieo__c where id=:currentRecordId];
        return vieoList;
    }
}