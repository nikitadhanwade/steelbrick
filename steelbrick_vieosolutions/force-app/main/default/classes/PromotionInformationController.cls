public class PromotionInformationController {
    
    public static string currentRecordId{get;set;}
    public static list<Integer> dummyList {get;set;}
    
    public PromotionInformationController(){
        dummyList = new list<Integer>();
        dummyList.add(1);
    }
    public static List<PromotionWrapper> getPromotionList(){
        list<Promotion__c> allPromotionRecordList = new list<Promotion__c>();
        list<Promotion__c> labPromotionRecordList = new list<Promotion__c>();
        list<Product_Promotion__c> prodPromotionRecordList = new list<Product_Promotion__c>();
        list<Promotion__c> promotionRecordList = new list<Promotion__c>();
        list<Promotion__c> productPromotionRecordList = new list<Promotion__c>();
        
        set<Id> labPromotionIdSet = new set<Id>();
        set<Id> prodPromotionIdSet = new set<Id>();
        
        // Getting all Lab Promotions for Vieo
        for (Lab_Promotion__c labPromotion :getLabPromotion(currentRecordId)) {
            if (labPromotion.Promotion__c == null) {
                continue;
            }
            labPromotionIdSet.add(labPromotion.Promotion__c);   
        }
        
        // Getting all Product Promotions for Vieo
        for (Product_Promotion__c productPromotion :getProductPromotion(currentRecordId)) {
            System.debug('******productPromotion******'+productPromotion);
            if (productPromotion.Id == null) {
                continue;
            }
            prodPromotionIdSet.add(productPromotion.Id);  
        }
        
        System.debug('******prodPromotionIdSet******'+prodPromotionIdSet);
        //1.promotions that are linked to Vieo object (office) using lab Promotion object
        if(!labPromotionIdSet.isEmpty()) {
            labPromotionRecordList = [Select id, Discount_Amount__c,Discount__c,End_Date__c,Months_Free__c,Name,Start_Date__c from Promotion__c Where Id IN:labPromotionIdSet AND End_Date__c>=: System.today() ORDER by Start_Date__c desc,End_Date__c desc,Name asc];
         }
            
        //2.Show Promotions linked to products associated with vieo object (product needs to active), also the promotion start date should be today or greater than today
        if(!prodPromotionIdSet.isEmpty()) 
            //prodPromotionRecordList = [Select id, Discount_Amount__c,Discount__c,End_Date__c,Months_Free__c,Name,Start_Date__c from Promotion__c Where Id IN:prodPromotionIdSet AND End_Date__c>=: System.today() ORDER by Start_Date__c desc,End_Date__c desc,Name asc];
            prodPromotionRecordList = [SELECT Promotion__r.Discount__c
                                       , Promotion__r.End_Date__c
                                       , Promotion__r.Start_Date__c
                                       , Promotion__r.Name
                                       , Promotion__r.Months_Free__c
                                       , Promotion__r.Discount_Amount__c
                                       , Promotion__c
                                       , Product__c
                                       , Product__r.Name FROM Product_Promotion__c where Vieo_id__c=:currentRecordId
                                      AND Promotion__r.End_Date__c>=: System.today() ORDER by Promotion__r.Start_Date__c desc,Promotion__r.End_Date__c desc,Promotion__r.Name asc];
        //3.promotions that are not linked to vieo or product object 
        
            
      System.debug('**********prodPromotionRecordList*****************'+prodPromotionRecordList);
        List<PromotionWrapper> promotionWrapperList = new List<PromotionWrapper>();
        
       
            for(Promotion__c promotion :getAllPromotion()) {
                if(promotion.Product_Promotion__r.size()>0){ 
                    promotionWrapperList.add(new PromotionWrapper(promotion, 'Global',promotion.Product_Promotion__r[0].product__r.name));
                }else
                {
                    promotionWrapperList.add(new PromotionWrapper(promotion, 'Global',null));
                }
            }
            //allPromotionRecordList.addAll(promotionRecordList);
        
        if(labPromotionRecordList.size()>0) {
            for(Promotion__c labPromotion :[Select id, Discount_Amount__c,Discount__c,End_Date__c,Months_Free__c,Name,Start_Date__c from Promotion__c Where Id IN:labPromotionIdSet AND End_Date__c>=: System.today() ORDER by Start_Date__c desc,End_Date__c desc,Name asc]) {
                if(labPromotion.Product_Promotion__r.size()>0){ 
                    promotionWrapperList.add(new PromotionWrapper(labPromotion, 'Lab',labPromotion.Product_Promotion__r[0].product__r.name));
                }else{
                    promotionWrapperList.add(new PromotionWrapper(labPromotion, 'Lab',null));
                }
            }
            //allPromotionRecordList.addAll(promotionRecordList);
        }
       /* if(prodPromotionRecordList.size()>0) {
            //for(Promotion__c productPromotion :[Select id, Discount_Amount__c,Discount__c,End_Date__c,Months_Free__c,Name,Start_Date__c,(select product__r.name from Product_Promotion__r) from Promotion__c Where Id IN:prodPromotionIdSet AND End_Date__c>=: System.today() ORDER by Start_Date__c desc,End_Date__c desc,Name asc]) {
            for(Product_Promotion__c productPromotion : prodPromotionRecordList) {
                if(productPromotion.size()>0){
                    promotionWrapperList.add(new PromotionWrapper(productPromotion, 'Product',productPromotion.product__r.name));
                }else{
                    promotionWrapperList.add(new PromotionWrapper(productPromotion, 'Product',null));
                }
            }
            //allPromotionRecordList.addAll(promotionRecordList);
        }*/
        for(Product_Promotion__c productPromotion : prodPromotionRecordList) {
            Promotion__c prom = new Promotion__c();
            prom.Start_Date__c=productPromotion.Promotion__r.Start_Date__c;
            prom.Discount__c=productPromotion.Promotion__r.Discount__c;
            prom.End_Date__c=productPromotion.Promotion__r.End_Date__c;
            prom.Start_Date__c=productPromotion.Promotion__r.Start_Date__c;
            prom.Months_Free__c=productPromotion.Promotion__r.Months_Free__c;
            prom.Name= productPromotion.Promotion__r.Name;
            prom.Discount_Amount__c=productPromotion.Promotion__r.Discount_Amount__c;
            productPromotionRecordList.add(prom);
            if(productPromotionRecordList.size()>0){
                    promotionWrapperList.add(new PromotionWrapper(prom, 'Product',productPromotion.Product__r.Name));
                }else{
                    promotionWrapperList.add(new PromotionWrapper(prom, 'Product',null));
                }
        }
        return promotionWrapperList;
    }
    
    public static List<Vieo__c> getvieoList(){
        list<Vieo__c> vieoList = [SELECT Name FROM Vieo__c where id=:currentRecordId];
        return vieoList;
    }
    
    private static List<Promotion__c> getAllPromotion() {
        return [SELECT id,Discount_Amount__c,Discount__c,End_Date__c,Months_Free__c,Name,Start_Date__c
                                   FROM Promotion__c 
                                   where Id NOT IN (SELECT Promotion__c FROM Lab_Promotion__c) 
                                   AND Id NOT IN (SELECT Promotion__c FROM Product_Promotion__c) AND
                                   End_Date__c>=: System.today() ORDER by Start_Date__c desc,End_Date__c desc,Name asc];
    }
    
    private static List<Lab_Promotion__c> getLabPromotion(String currentRecordId) {
        return [Select Promotion__c  from Lab_Promotion__c where Vieo__c =:currentRecordId];
    }
    
    private static List<Product_Promotion__c> getProductPromotion(String currentRecordId) {
        return [Select Promotion__c  from Product_Promotion__c where Vieo_Id__c =:currentRecordId];
    }
  
    public class PromotionWrapper {
        Public Promotion__c promotion { get; set; }
        public String promotionType { get; set; } 
        Public String productName { get; set; }
        public PromotionWrapper(Promotion__c promotion, String promotionType,String productName) {
            this.promotion = promotion;
            this.promotionType = promotionType;
            this.productName = productName;
        }
    }
   
}