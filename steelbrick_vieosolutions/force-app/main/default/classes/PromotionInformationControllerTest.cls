@Istest
public class PromotionInformationControllerTest {

    @istest static void testGetPromotionList(){
    
        
        List<PromotionInformationController.PromotionWrapper> prom = new List<PromotionInformationController.PromotionWrapper>();
        List<Vieo__c> vieolist = new List<Vieo__c>();
        test.startTest();
        PriceBook2 pb2=new PriceBook2();
        pb2.Name = 'test';
        pb2.IsActive = true;
        insert pb2;

        Vieo__c vieo = new Vieo__c();
        vieo.Name = 'california';
        vieo.Price_Book__c = pb2.id;
        insert vieo;
       
        Product2 pro = new Product2();
        pro.Name = 'Test 1';
        pro.Category__c = 'Non Resident Memberships';
        pro.ProductCode = '100';
        pro.Description = 'For testing 1';
        pro.Capacity__c = '5';
        pro.SQ_FT__c = 250;
        pro.Location__c = 'window';
        pro.Vieo__c = vieo.Id;
        insert pro;

       
        Promotion__c promotion = new Promotion__c();
        promotion.Discount_Amount__c = 15.00;
        promotion.Discount__c = 10;
        promotion.End_Date__c = Date.parse('07/19/2018');
        promotion.Months_Free__c = 2;
        promotion.Name = 'two month free';
        promotion.Start_Date__c = Date.parse('06/19/2018');
        insert promotion;
        
        Promotion__c promotion1 = new Promotion__c();
        promotion1.Discount_Amount__c = 15.00;
        promotion1.Discount__c = 10;
        promotion1.End_Date__c = Date.parse('07/19/2032');
        promotion1.Months_Free__c = 2;
        promotion1.Name = 'two month free';
        promotion1.Start_Date__c = Date.parse('06/19/2018');
        insert promotion1;
        
        Lab_Promotion__c labPromotion = new Lab_Promotion__c();
        labPromotion.Promotion__c = promotion.Id;
        labPromotion.Vieo__c = vieo.id;
        insert labPromotion;
        
        Product_Promotion__c productPromotion = new Product_Promotion__c();
        productPromotion.Product__c = pro.id;
        productPromotion.Promotion__c = promotion.id;
        
        insert productPromotion;
        
        List<Product_Promotion__c> prodpromotion = [select Vieo_Id__c from Product_Promotion__c where id =: productPromotion.Id];
        PromotionInformationController promotionInfo = new PromotionInformationController();
        PromotionInformationController.currentRecordId=prodpromotion[0].Vieo_Id__c;
        prom=PromotionInformationController.getPromotionList();
        vieolist=PromotionInformationController.getvieoList();
        test.stopTest();
    }    
}