global class QuoteIntegration{
    
    global class getQouteResponse{
        webservice String msg;
    }
    global class getQouteRequest {
        webservice String TransSetIDCode {get;set;}
        webservice String TransControlID {get;set;}
        webservice String ResponseVersion {get;set;}
        webservice String RefIDQual {get;set;}
        webservice String RefID {get;set;}
        //webservice String AccountId {get;set;}
        
        public getQouteRequest(String TransSetIDCode,String TransControlID,String ResponseVersion,String RefIDQual,String RefID,String AccountId) {
            this.TransSetIDCode = TransSetIDCode;
            this.TransControlID = TransControlID;
            this.ResponseVersion = ResponseVersion;
            this.RefIDQual = RefIDQual;
            this.RefID = RefID;
            //this.AccountId = AccountId;
        }
    }
    webservice static void xmlSubmit(String techDataQuoteNumber)
    { 
        System.debug('In Apex Class');
        System.debug('techDataQuoteNumber-------------:'+techDataQuoteNumber);
        string xml = '<XML_Quote_Submit><Header><UserName>700103</UserName><Password>!testP@ss58601</Password><TransSetIDCode>812</TransSetIDCode><TransControlID>10000</TransControlID><ResponseVersion>1.0</ResponseVersion></Header><Detail><RefInfo><RefIDQual>QT</RefIDQual><RefID>'+techDataQuoteNumber+'</RefID></RefInfo></Detail><Summary><NbrOfSegments /></Summary></XML_Quote_Submit>';
        String endPointURL = 'https://tdxml.cstenet.com/xmlservlet';
        Httprequest request = new HttpRequest();
        Http http = new Http();
        request.setMethod('POST');
        request.setEndpoint(endPointURL);
        request.setHeader('Content-Type', 'application/json');
        request.setTimeout(120000); 
        request.setBody(xml);          
        HttpResponse response = http.send(request);  
        XmlStreamReader reader = response.getXmlStreamReader();
        String xml1 = response.getBody();
        system.debug('xml1-------'+xml1);
       // String xml1 ='<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE XML_Quote_Response SYSTEM "XML_Quote_Response.dtd"><XML_Quote_Response><Header><TransSetIDCode>812</TransSetIDCode><TransControlID>10000</TransControlID><ResponseVersion>1.0</ResponseVersion></Header><Detail><RefInfo><RefIDQual>QT</RefIDQual><RefID>4002553450</RefID></RefInfo><Market>Commercial</Market><EntryDate>12/04/18</EntryDate><ShipToName>TECH DATA CORPORATION</ShipToName><ShipToAddr1>5350 Tech Data Dr</ShipToAddr1><ShipToAddr2/><ShipToAddr3/><ShipToCity>CLEARWATER</ShipToCity><ShipToStateProv>FL</ShipToStateProv><ShipToPostalCode>33760-3122</ShipToPostalCode><ContactName/><EndUserInfo><EuiName>PWC - PUBLIC LIBRARY SYSTEM</EuiName><EuiCountryCode>US</EuiCountryCode><EuiEndUserRef3>PB86671729FO</EuiEndUserRef3></EndUserInfo><LineInfo><LineNumber>100</LineNumber><ProductIDQual>VP</ProductIDQual><ProductID>10108727</ProductID><ProductIDQual2>MG</ProductIDQual2><ProductID2>15216-ATT-LC-10=</ProductID2><ProductDesc>CISCO 15216 ONS BULK ATT LC CONN 10DB</ProductDesc><Manufacturer>Cisco Systems</Manufacturer><MSRP>$200.00</MSRP><UnitPrice>$123.03</UnitPrice><ExtendedPrice>$2,952.72</ExtendedPrice><QtyOrdered>24</QtyOrdered></LineInfo><LineInfo><LineNumber>100</LineNumber><ProductIDQual>VP</ProductIDQual><ProductID>10108727</ProductID><ProductIDQual2>MG</ProductIDQual2><ProductID2>15216-ATT-LC-10=</ProductID2><ProductDesc>CISCO 15216 ONS BULK ATT LC CONN 10DB</ProductDesc><Manufacturer>Cisco Systems</Manufacturer><MSRP>$200.00</MSRP><UnitPrice>$123.03</UnitPrice><ExtendedPrice>$2,952.72</ExtendedPrice><QtyOrdered>24</QtyOrdered></LineInfo><NbrOfOrderLines>1</NbrOfOrderLines></Detail></XML_Quote_Response>';
        //system.debug('xml1-------'+xml1);
        Dom.Document doc = new Dom.Document();
        doc.load(xml1);
        Dom.XMLNode ordDtls = doc.getRootElement();
        //System.debug('ordDtls: ' + ordDtls);
        String quoteid,Password,Market,EntryDate,ShipToName,ShipToAddr1,ShipToAddr2,ShipToAddr3,ShipToCity,ShipToStateProv,ShipToPostalCode,ContactName;
        String LineNumber,ProductID,ProductID2,ProductDesc,Manufacturer,ProductIDQual2,ProductIDQual,UnitPrice,QtyOrdered;
       String MSRP;
       List<String> listOfData = new List<String>();
        String TransSetIDCode;
       String TransControlID;
        String ResponseVersion;
        String RefId;
        String RefIdQual;
        //String Market;
         List<Dom.XmlNode> xrecs = ordDtls.getChildelements();
        System.debug('xrecs----==-=-=-='+xrecs);
        for(Dom.XMLNode child : xrecs){
            
            Dom.XMLNode errorChild = child.getChildElements()[0];
            
            if(TransSetIDCode==null){
            TransSetIDCode = errorChild.getText();
            System.debug('TransSetIDCode----==-=-=-='+TransSetIDCode);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild1 = child.getChildElements()[1];
            TransControlID = errorChild1.getText();
            System.debug('TransControlID----==-=-=-='+TransControlID);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild2 = child.getChildElements()[2];
            ResponseVersion = errorChild2.getText();
            System.debug('ResponseVersion----==-=-=-='+ResponseVersion);
            }
           break;
        }
        System.debug('TransSetIDCode: ' + TransSetIDCode);
        System.debug('TransControlID: ' +  TransControlID);
        System.debug('ResponseVersion: ' + ResponseVersion);
        
        Dom.XMLNode DetailNode = ordDtls.getChildElement('Detail', null);
        
         Market = DetailNode.getChildElement('Market', null).getText();
       
         EntryDate = DetailNode.getChildElement('EntryDate', null).getText();
        
         ShipToName = DetailNode.getChildElement('ShipToName', null).getText(); 
        
         ShipToAddr1 = DetailNode.getChildElement('ShipToAddr1', null).getText();
        
         ShipToAddr2 = DetailNode.getChildElement('ShipToAddr2', null).getText();
        
         ShipToAddr3 = DetailNode.getChildElement('ShipToAddr3', null).getText();
        
         ShipToCity = DetailNode.getChildElement('ShipToCity', null).getText();
        
         ShipToStateProv = DetailNode.getChildElement('ShipToStateProv', null).getText();
        
         ShipToPostalCode = DetailNode.getChildElement('ShipToPostalCode', null).getText();
        
         ContactName = DetailNode.getChildElement('ContactName', null).getText();
        
        /*System.debug('Market: ' + Market);
        System.debug('EntryDate: ' +  EntryDate);
        System.debug('ShipToName: ' +  ShipToName);
        System.debug('ShipToAddr1: ' + ShipToAddr1);
        System.debug('ShipToAddr2: ' +  ShipToAddr2);
        System.debug('ShipToAddr3: ' +  ShipToAddr3);
        System.debug('ShipToCity: ' + ShipToCity);
        System.debug('ShipToStateProv: ' +  ShipToStateProv);
        System.debug('ShipToPostalCode: ' +  ShipToPostalCode);
        System.debug('ContactName: ' +  ContactName);*/
        
         Dom.XMLNode RefInfo = DetailNode.getChildElement('RefInfo', null);
        RefIDQual = RefInfo.getChildElement('RefIDQual', null).getText();
        RefID = RefInfo.getChildElement('RefID', null).getText();
        
        //System.debug('RefIDQual: ' + RefIDQual);
        //System.debug('RefID: ' +  RefID);
        
        //Dom.XMLNode LineInfo = DetailNode.getChildElement('LineInfo', null); 
        /*Dom.XMLNode LineInfo = DetailNode.getChildElement('LineInfo', null);
        String LineNumber = LineInfo.getChildElement('LineNumber', null).getText();
        String ProductIDQual = LineInfo.getChildElement('ProductIDQual', null).getText();
        String ProductID = LineInfo.getChildElement('ProductID', null).getText();
        String ProductIDQual2 = LineInfo.getChildElement('ProductIDQual2', null).getText();
        String ProductID2 = LineInfo.getChildElement('ProductID2', null).getText();
        String ProductDesc = LineInfo.getChildElement('ProductDesc', null).getText();
        String Manufacturer = LineInfo.getChildElement('Manufacturer', null).getText();
        String MSRP = LineInfo.getChildElement('MSRP', null).getText();
        String UnitPrice = LineInfo.getChildElement('UnitPrice', null).getText();
        String ExtendedPrice = LineInfo.getChildElement('ExtendedPrice', null).getText();
        String QtyOrdered = LineInfo.getChildElement('QtyOrdered', null).getText();
        
        System.debug('LineNumber: ' + LineNumber);
        System.debug('ProductIDQual: ' +  ProductIDQual);
        System.debug('ProductID: ' +  ProductID);
        System.debug('ProductIDQual2: ' + ProductIDQual2);
        System.debug('ProductID2: ' +  ProductID2);
        System.debug('ProductDesc: ' +  ProductDesc);
        System.debug('Manufacturer: ' + Manufacturer);
        System.debug('MSRP: ' +  MSRP);
        System.debug('UnitPrice: ' +  UnitPrice);
        System.debug('ExtendedPrice: ' +  ExtendedPrice);
        System.debug('QtyOrdered: ' +  QtyOrdered);*/
        
        
        List<Dom.XmlNode> xrec = ordDtls.getChildelements();
        for(Dom.XMLNode child : xrec){
            
            List<Dom.XmlNode> xrec1 = child.getChildelements();
            System.debug('xrec1-----'+xrec1);
            Dom.XMLNode LineInfo = DetailNode.getChildElement('LineInfo', null);
        LineNumber = LineInfo.getChildElement('LineNumber', null).getText();
        ProductIDQual = LineInfo.getChildElement('ProductIDQual', null).getText();
        ProductID = LineInfo.getChildElement('ProductID', null).getText();
         ProductIDQual2 = LineInfo.getChildElement('ProductIDQual2', null).getText();
         ProductID2 = LineInfo.getChildElement('ProductID2', null).getText();
         ProductDesc = LineInfo.getChildElement('ProductDesc', null).getText();
         Manufacturer = LineInfo.getChildElement('Manufacturer', null).getText();
         MSRP = LineInfo.getChildElement('MSRP', null).getText();
         UnitPrice = LineInfo.getChildElement('UnitPrice', null).getText();
        String ExtendedPrice = LineInfo.getChildElement('ExtendedPrice', null).getText();
         QtyOrdered = LineInfo.getChildElement('QtyOrdered', null).getText();
            
             System.debug('listOfData: ' + listOfData);
        System.debug('LineNumber: ' + LineNumber);
        System.debug('ProductIDQual: ' +  ProductIDQual);
        System.debug('ProductID: ' +  ProductID);
        System.debug('ProductIDQual2: ' + ProductIDQual2);
        System.debug('ProductID2: ' +  ProductID2);
        System.debug('ProductDesc: ' +  ProductDesc);
        System.debug('Manufacturer: ' + Manufacturer);
        System.debug('MSRP: ' +  MSRP);
        System.debug('UnitPrice: ' +  UnitPrice);
        System.debug('ExtendedPrice: ' +  ExtendedPrice);
        System.debug('QtyOrdered: ' +  QtyOrdered);
            
           
            
            
            /*List<Dom.XMLNode> resultList = child.getChildElements();
            //Dom.XMLNode resultList = DetailNode.getChildElement('LineInfo', null);
            System.debug('result--------::'+resultList);
            System.debug('resultList::'+resultList.size());
            
            for(Dom.XMLNode LineInfo: resultList) {
                if(LineInfo.getName() == 'LineNumber') {
                    LineNumber = LineInfo.getText();
                    System.debug('LineNumber::'+LineNumber);
                }else if(LineInfo.getName() == 'ProductID') {
                    ProductID = LineInfo.getText();
                    System.debug('ProductID::'+ProductID);
                }else if(LineInfo.getName() == 'ProductID2') {
                    ProductID2 = LineInfo.getText();
                    System.debug('ProductID2::'+ProductID2);
                }else if(LineInfo.getName() == 'ProductDesc') {
                    ProductDesc = LineInfo.getText();
                    System.debug('ProductDesc::'+ProductDesc);
                }else if(LineInfo.getName() == 'Manufacturer') {
                    Manufacturer = LineInfo.getText();
                    System.debug('Manufacturer::'+Manufacturer);
                }else if(LineInfo.getName() == 'LineNumber') {
                    LineNumber = LineInfo.getText();
                    System.debug('LineNumber::'+LineNumber);
                }else if(LineInfo.getName() == 'ProductDesc') {
                    ProductDesc = LineInfo.getText();
                    System.debug('ProductDesc::'+ProductDesc);
                }else if(LineInfo.getName() == 'ProductIDQual2') {
                    ProductIDQual2 = LineInfo.getText();
                    System.debug('ProductIDQual2::'+ProductIDQual2);
                }else if(LineInfo.getName() == 'ProductIDQual') {
                    ProductIDQual = LineInfo.getText();
                    System.debug('ProductIDQual::'+ProductIDQual);
                }else if(LineInfo.getName() == 'MSRP') {
                    MSRP = LineInfo.getText();
                    System.debug('MSRP::'+MSRP);
                }else if(LineInfo.getName() == 'UnitPrice') {
                    UnitPrice = LineInfo.getText();
                    System.debug('UnitPrice::'+UnitPrice);
                }else if(LineInfo.getName() == 'QtyOrdered') {
                    QtyOrdered = LineInfo.getText();
                    System.debug('QtyOrdered::'+QtyOrdered);
                }
            }*/
        }   
        
        
        List<Product2> pro=[select id, name, isActive,ProductCode from Product2 where ProductCode =: ProductID limit 1];
        System.debug('pro-------'+pro);
        
        Product2 prod = new Product2();
        PricebookEntry pbk1 = new PricebookEntry();
        if(pro.size()==0){
            
            prod.Name=ProductID2;
            prod.Description=ProductDesc;
            prod.IsActive=true;
            prod.ProductCode=ProductID;
            prod.Manufacturer__c=Manufacturer;
            insert prod;
            
            Pricebook2 pb2 = [select Id, Name, IsActive from PriceBook2 where IsStandard=True LIMIT 1];
            
            //pbk1.Name=Manufacturer;
            pbk1.Pricebook2Id = pb2.Id; 
            pbk1.Product2Id = prod.Id;
            pbk1.UnitPrice = 0; 
            pbk1.isActive=true;
            
            
            insert pbk1;
        }
        
       
        
       
        
        List<SBQQ__Quote__c> qt =[SELECT Id,Name from SBQQ__Quote__c where TechData_Quote_Number__c=:techDataQuoteNumber];
        System.debug('qt----'+qt);
        List<SBQQ__Quote__c> quoteListToUpdate = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c updateQuote:qt){
            updateQuote.TransSetIDCode__c=String.valueOf(TransSetIDCode);
            updateQuote.TransControlID__c=String.valueOf(TransControlID);
        	updateQuote.ResponseVersion__c=String.valueOf(ResponseVersion);
            updateQuote.RefIDQual__c=RefIDQual;
            updateQuote.TechData_Quote_Number__c=techDataQuoteNumber;
            updateQuote.Market__c=Market;
            updateQuote.EntryDate__c=Date.parse(EntryDate);
            updateQuote.SBQQ__ShippingName__c=ShipToName;
            updateQuote.SBQQ__ShippingStreet__c=ShipToAddr1;
            updateQuote.SBQQ__ShippingCity__c=ShipToCity;
            updateQuote.SBQQ__ShippingState__c=ShipToStateProv;
            updateQuote.SBQQ__ShippingPostalCode__c=ShipToPostalCode;
            updateQuote.ContactName__c=ContactName;
            quoteListToUpdate.add(updateQuote); 
        }
        if(quoteListToUpdate.size() != 0 && !quoteListToUpdate.isEmpty()){
            
             Database.SaveResult[] results  = Database.update(quoteListToUpdate);
        }
        
        SBQQ__QuoteLine__c qliliner = new SBQQ__QuoteLine__c();
        for(SBQQ__Quote__c quote:qt){
            
            System.debug('quote-----'+quote.Id);
            quoteid=quote.Id;
            quoteid=quoteid.substring(0,15);
            System.debug('quoteid-----'+quoteid);
            if(qt.size()>0){
                
                qliliner.SBQQ__Number__c=Decimal.valueOf(LineNumber);
                qliliner.ProductIDQual__c=ProductIDQual;
                if(pro.size()==0){
                    qliliner.SBQQ__Product__c=prod.Id;
                }else{
                    for(Product2 product:pro){
                        qliliner.SBQQ__Product__c=product.Id;
                    }
                    
                }
                qliliner.ProductIDQual2__c=ProductIDQual2;
                qliliner.MSRP__c=String.valueof(MSRP);
                qliliner.UnitPrice__c=UnitPrice;
                qliliner.SBQQ__Quantity__c=Decimal.valueOf(QtyOrdered);
                qliliner.SBQQ__Quote__c=quote.Id;
                System.debug('String.valueof(MSRP)------'+String.valueof(MSRP));
                MSRP=MSRP.substring(1, MSRP.length());
                MSRP=MSRP.replaceAll(',', '');
                System.debug('MSRP------'+MSRP);
                //MSRP=MSRP.replaceAll('\\D','');
                qliliner.SBQQ__ListPrice__c=Decimal.valueOf(MSRP);
               // UnitPrice=UnitPrice.replaceAll('\\D','');
               System.debug('UnitPrice------'+UnitPrice);
                UnitPrice=UnitPrice.substring(1, UnitPrice.length());
                
                System.debug('UnitPrice1------'+UnitPrice);
                qliliner.SBQQ__NetPrice__c=Decimal.valueOf(UnitPrice);
                Decimal Additionalprice=(Decimal.valueOf(MSRP)-Decimal.valueOf(UnitPrice));
                qliliner.SBQQ__AdditionalDiscountAmount__c= Additionalprice;
                qliliner.SBQQ__Description__c=ProductDesc;

                insert qliliner;
                
            }
        }
        
      
                    //res.responseBody = Blob.valueOf(retBody);
                    /* Dom.XmlNode methodResponse = doc.getRootElement();
            
            List<Dom.XmlNode> dataNodes = methodResponse.getChildElement('Detail', null).getChildElements();
            
            
            for(Dom.XmlNode mNode : dataNodes){
            List<Dom.XmlNode> memberNodes = mNode.getChildElement('LineInfo', null)
            .getChildElements();
            
            for(Dom.XmlNode mNode1 : memberNodes){
            
            String LineNumber = mNode1.getChildElement('LineNumber', null).getText();
            System.debug('LineNumber: ' + LineNumber);
            
            }
            }*/
                    
                    
                    
                    /*  for(Dom.XMLNode child : LineInfo.getChildElements()) {
            //Dom.XMLNode errorChild = child.getChildElements()[1];
            //System.debug('child----==-=-=-='+child);
            Dom.XMLNode LineInfo = DetailNode.getChildElement('LineInfo', null);
            String LineNumber = LineInfo.getChildElement('LineNumber', null).getText();
            String ProductIDQual = LineInfo.getChildElement('ProductIDQual', null).getText();
            String ProductID = LineInfo.getChildElement('ProductID', null).getText();
            String ProductIDQual2 = LineInfo.getChildElement('ProductIDQual2', null).getText();
            String ProductID2 = LineInfo.getChildElement('ProductID2', null).getText();
            String ProductDesc = LineInfo.getChildElement('ProductDesc', null).getText();
            String Manufacturer = LineInfo.getChildElement('Manufacturer', null).getText();
            String MSRP = LineInfo.getChildElement('MSRP', null).getText();
            String UnitPrice = LineInfo.getChildElement('UnitPrice', null).getText();
            String ExtendedPrice = LineInfo.getChildElement('ExtendedPrice', null).getText();
            String QtyOrdered = LineInfo.getChildElement('QtyOrdered', null).getText();
            
            System.debug('LineNumber: ' + LineNumber);
            System.debug('ProductIDQual: ' +  ProductIDQual);
            System.debug('ProductID: ' +  ProductID);
            System.debug('ProductIDQual2: ' + ProductIDQual2);
            System.debug('ProductID2: ' +  ProductID2);
            System.debug('ProductDesc: ' +  ProductDesc);
            System.debug('Manufacturer: ' + Manufacturer);
            System.debug('MSRP: ' +  MSRP);
            System.debug('UnitPrice: ' +  UnitPrice);
            System.debug('ExtendedPrice: ' +  ExtendedPrice);
            System.debug('QtyOrdered: ' +  QtyOrdered);
            /*if(Market==null){
            String res = errorChild.getName();
            if(Market==res){
            Market = errorChild.getText();
            System.debug('Market----==-=-=-='+Market);
            }
            }
            if (child.getChildElements().size() > 1) {
            
            Dom.XMLNode errorChild1 = child.getChildElements()[2];
            TransControlID = errorChild1.getText();
            System.debug('TransControlID----==-=-=-='+TransControlID);
            }
            
            
            
            
            
            */
                    //} 
                    
                    
                    
                    /*  String responseXML = '';
            responseXML  += '<response>';
            XmlStreamWriter w = new XmlStreamWriter();
            w.writeStartElement(null , 'Message', null );
            w.writeCharacters('<XML_Quote_Submit> <Header> <UserName>700103</UserName> <Password>!testP@ss58601</Password> <TransSetIDCode>812</TransSetIDCode> <TransControlID>10000</TransControlID> <ResponseVersion>1.0</ResponseVersion> </Header> <Detail> <RefInfo> <RefIDQual>QT</RefIDQual> <RefID>4002553450</RefID> </RefInfo> </Detail> <Summary> <NbrOfSegments /> </Summary> </XML_Quote_Submit>');
            w.writeEndElement();
            
            responseXML += '</response>';
            String xmlOutput = '<?xml version="1.0" encoding="UTF-8"?>' + '<Response>' + w.getXmlString() 
            + '</Response>';
            w.close();*/
                    // return xmlOutput ;
                    
                    /* String Password10 = errorChild.getText();
            String Password1 = errorChild.getName();
            String Password2 = errorChild.getNamespace();
            String Password3 = String.valueOf(errorChild.getNodeType());
            String Password4 = String.valueOf(errorChild.getParent());
            
            System.debug('Password10----==-=-=-='+Password10);
            System.debug('Password1----==-=-=-='+Password1);
            System.debug('Password2----==-=-=-='+Password2);
            System.debug('Password3----==-=-=-='+Password3);
            System.debug('Password4----==-=-=-='+Password4);*/
                    
                    
                    /*string xml = '<XML_Quote_Submit><Header><UserName>700078</UserName><Password>!testP@ss58601</Password><TransSetIDCode>812</TransSetIDCode><TransControlID>10000</TransControlID><ResponseVersion>1.0</ResponseVersion></Header><Detail><RefInfo><RefIDQual>QT</RefIDQual><RefID>4002553450</RefID></RefInfo></Detail><Summary><NbrOfSegments /></Summary></XML_Quote_Submit>';
            Dom.Document doc = new Dom.Document();
            doc.load(xml);
            Dom.XMLNode ordDtls = doc.getRootElement();
            System.debug('ordDtls: ' + ordDtls);
            String Password;
            String TransSetIDCode;
            String TransControlID;
            String ResponseVersion;
            String RefId;
            String RefIdQual;
            String UserName;
            for(Dom.XMLNode child : ordDtls.getChildElements()) {
            Dom.XMLNode errorChild = child.getChildElements()[0];
            
            if(UserName==null){
            UserName = errorChild.getText();
            System.debug('UserName----==-=-=-='+UserName);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild1 = child.getChildElements()[1];
            Password = errorChild1.getText();
            System.debug('Password----==-=-=-='+Password);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild2 = child.getChildElements()[2];
            TransSetIDCode = errorChild2.getText();
            System.debug('TransSetIDCode----==-=-=-='+TransSetIDCode);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild3 = child.getChildElements()[3];
            TransControlID = errorChild3.getText();
            System.debug('TransControlID----==-=-=-='+TransControlID);
            }
            if (child.getChildElements().size() > 1) {
            Dom.XMLNode errorChild4 = child.getChildElements()[4];
            ResponseVersion = errorChild4.getText();
            System.debug('ResponseVersion----==-=-=-='+ResponseVersion);
            }
            
            
            if (errorChild.getChildElements().size() > 1) {
            Dom.XMLNode errorChildSecond = errorChild.getChildElements()[0];
            Dom.XMLNode errorChildSecond1 = errorChild.getChildElements()[1];
            
            RefIdQual = errorChildSecond.getText();
            RefId = errorChildSecond1.getText();
            
            System.debug('RefId----==-=-=-='+RefId);
            System.debug('RefIdQual----==-=-=-='+RefIdQual);
            
            }
            
            } 
            
            String endPointURL = 'https://tdxml.cstenet.com/xmlservlet';
            String userName1 = '700103';
            String password1 = '!testP@ss58601';
            
            Blob headerValue = Blob.valueOf(userName1 + ':' + password1);
            String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
            
            Httprequest request = new HttpRequest();
            Http http = new Http();
            
            request.setMethod('POST');
            request.setEndpoint(endPointURL);
            request.setHeader('Content-Type', 'application/json');
            
            request.setHeader('Authorization', authorizationHeader);
            
            request.setTimeout(120000); 
            
            JSONGenerator gen = JSON.createGenerator(true);
            
            gen.writeStartObject();
            gen.writeStringField('UserName', UserName);
            gen.writeStringField('Password', Password);
            gen.writeStringField('TransSetIDCode', TransSetIDCode);
            gen.writeStringField('TransControlID', TransControlID);
            gen.writeStringField('ResponseVersion', ResponseVersion);
            gen.writeStringField('RefID', RefID);
            gen.writeStringField('RefIdQual', RefIdQual);
            
            String pretty = gen.getAsString();
            System.debug('pretty: '+pretty);
            request.setBody(pretty);          
            
            HttpResponse response = http.send(request);  
            
            System.debug('responseBody: '+response.getBody());*/
        
        
    }
}