public class ResidentProductInformationController {
    public static string currentRecordId{get;set;}
    //public static list<product2> productList {get;set;}
    public static list<Integer> dummyList {get;set;}
    public List<ResidentProductWrapper> residentProductList2 { get; set; }
    public list<Integer> MonthList {get;set;}
    
    public ResidentProductInformationController(){
        dummyList = new list<Integer>();
        dummyList.add(1);
        residentProductList2 = new List<ResidentProductWrapper>();
        
        
    }
    
    public List<ResidentProductWrapper> getResidentProductList(){
         residentProductList2 = new List<ResidentProductWrapper>();
         ResidentProductWrapper.vieolabid = currentRecordId;
         Map<Decimal,Decimal> discountMap=ResidentProductWrapper.getDiscountMap();
       Vieo__c objVieo =[SELECT Name,Price_Book__c FROM Vieo__c where id=:currentRecordId];
        
        //for(product2 productList : [select name,Category__c,ProductCode,Description,Capacity__C,SQ_FT__c,Location__c,(SELECT UnitPrice FROM PricebookEntries where pricebook2id=:objVieo.price_book__c)  from product2 where Vieo__c=:currentRecordId AND Category__c =: Label.Resident ORDER BY ProductCode]){
        for(PricebookEntry pricebookEntryList : [SELECT Product2.ProductCode,Product2.Name,Product2.Description,Product2.Capacity__C,Product2.SQ_FT__c,Product2.Location__c,UnitPrice FROM PricebookEntry where pricebook2id =:objVieo.price_book__c AND Product2.Category__c =: Label.Resident AND Product2.Vieo__c=:currentRecordId AND Product2.IsActive	= true ORDER BY Product2.ProductCode]){
            
            residentProductList2.add(new ResidentProductWrapper(pricebookEntryList,discountMap)); 
        }
       // system.debug('residentProductList---------)'+residentProductList2);
         MonthList = new list<Integer>();
        if(discountMap.size()>0){
            for(Decimal discount : discountMap.keySet()){
                MonthList.add(Integer.valueOf(discount));
                
            }
            MonthList.sort();
        }
       //system.debug('MonthList---------)'+MonthList);
       
        return residentProductList2;
    }
    
    public static List<Vieo__c> getvieoList(){
        list<Vieo__c> vieoList = [SELECT Name FROM Vieo__c where id=:currentRecordId];
       // System.debug('currentRecordId'+currentRecordId);
        return vieoList;
    }
    
}