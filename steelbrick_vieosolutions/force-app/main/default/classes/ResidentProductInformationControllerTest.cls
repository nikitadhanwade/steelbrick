@istest
public class ResidentProductInformationControllerTest {

    @istest static void testGetResidentProductList(){

        List<ResidentProductWrapper> resident = new List<ResidentProductWrapper>();
        List<Vieo__c> vieolist = new List<Vieo__c>();
        PriceBook2 pb2=new PriceBook2();
        pb2.Name = 'test';
        pb2.IsActive = true;
        insert pb2;
        
        Vieo__c vieo = new Vieo__c();
        vieo.Name = 'california';
        vieo.Price_Book__c = pb2.id;
        insert vieo;
        
        Id pricebookId = Test.getStandardPricebookId();

        Product2 pro = new Product2();
        pro.Name = 'Test 1';
        pro.Category__c = 'Resident Memberships';
        pro.ProductCode = '100';
        pro.Description = 'For testing 1';
        pro.Capacity__c = '5';
        pro.SQ_FT__c = 250;
        pro.Location__c = 'window';
        pro.Vieo__c = vieo.Id;
        insert pro;
        
        Product2 pro1 = new Product2();
        pro1.Name = 'Test 2';
        pro1.Category__c = 'Resident Memberships';
        pro1.ProductCode = '200';
        pro1.Description = 'For testing 2';
        pro1.Capacity__c = '10';
        pro1.SQ_FT__c = 350;
        pro1.Location__c = 'window';
        pro1.Vieo__c = vieo.Id;
        insert pro1;
        
        Product2 pro2 = new Product2();
        pro2.Name = 'Test 2';
        pro2.Category__c = 'Resident Memberships';
        pro2.ProductCode = '200';
        pro2.Description = 'For testing 2';
        pro2.Capacity__c = '10';
        pro2.SQ_FT__c = 350;
        pro2.Location__c = 'window';
        pro2.Vieo__c = vieo.Id;
        insert pro2;
        
        Product2 pro3 = new Product2();
        pro3.Name = 'Test 2';
        pro3.Category__c = 'Resident Memberships';
        pro3.ProductCode = '200';
        pro3.Description = 'For testing 2';
        pro3.Capacity__c = '10';
        pro3.SQ_FT__c = 350;
        pro3.Location__c = 'window';
        pro3.Vieo__c = vieo.Id;
        insert pro3;
        
        Product2 pro4 = new Product2();
        pro4.Name = 'Test 2';
        pro4.Category__c = 'Resident Memberships';
        pro4.ProductCode = '200';
        pro4.Description = 'For testing 2';
        pro4.Capacity__c = '10';
        pro4.SQ_FT__c = 350;
        pro4.Location__c = 'window';
        pro4.Vieo__c = vieo.Id;
        insert pro4;
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 3640, IsActive = true);
        insert standardPrice;
        
        PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro1.Id,
            UnitPrice = 1800, IsActive = true);
        insert standardPrice1;
        
        PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro2.Id,
            UnitPrice = 870, IsActive = true);
        insert standardPrice2;
        
        PricebookEntry standardPrice3 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro3.Id,
            UnitPrice = 520, IsActive = true);
        insert standardPrice3;
        
        PricebookEntry standardPrice4 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro4.Id,
            UnitPrice = 2590, IsActive = true);
        insert standardPrice4;
        
        PricebookEntry standardPrice5 = new PricebookEntry(
            Pricebook2Id = vieo.Price_Book__c, Product2Id = pro4.Id,
            UnitPrice = 3670, IsActive = true);
        insert standardPrice5;
        
        Term_Discount__c termdiscount = new Term_Discount__c(
        Name='1 Month',Markup_Discount__c=10,Term__c=1,Lookup_Price__c=true,vieo__c=vieo.Id
        );
            insert termdiscount;
         Term_Discount__c termdiscount1 = new Term_Discount__c(
        Name='2 Month',Markup_Discount__c=0,Term__c=2,Lookup_Price__c=true,vieo__c=vieo.Id
        );
            insert termdiscount1;
        Term_Discount__c termdiscount2 = new Term_Discount__c(
        Name='3 Month',Markup_Discount__c=-5,Term__c=3,Lookup_Price__c=true,vieo__c=vieo.Id
        );
            insert termdiscount2;
        
        ResidentProductInformationController NP = new ResidentProductInformationController();
        ResidentProductInformationController.currentRecordId=vieo.id;
        resident=NP.getResidentProductList();
        vieolist=ResidentProductInformationController.getvieoList();
    }
}