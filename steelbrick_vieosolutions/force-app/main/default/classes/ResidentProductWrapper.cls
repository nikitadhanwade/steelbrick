public class ResidentProductWrapper {
    
    public static string vieolabid;
    public PricebookEntry pricebookEntry {get;set;}
    public decimal months{get;set;}
    Map<Decimal,Decimal> discountMap{get;set;}
    public Map<Integer,Decimal> monthDiscountMap{get;set;}
    public Set<Integer> setStrings = new Set<Integer>();
    public Set<Integer> keyValues { get;  set; }
    //public decimal discount;
    public ResidentProductWrapper(PricebookEntry pro,Map<Decimal,Decimal> discountMap)
    {
        pricebookEntry=pro;
        //System.debug('discountMap------)'+discountMap);
        if(discountMap.size()>0){
            for(Decimal discount : discountMap.keySet()){
                
                if(pro !=null){
                      System.debug('pro.UnitPrice-------------'+pro.UnitPrice);            
                    monthDiscountMap =monthDiscountMap == null ? new Map<Integer,Decimal>() : monthDiscountMap;
                    Integer disc = (Integer)discountMap.get(discount);
                    if(disc == 0) {
                        //months = Math.round(pro.UnitPrice);
                        months = pro.UnitPrice;
                    }else if(disc<0){
                        
                        String surcharge= String.valueOf(discountMap.get(discount)/100);
                        Decimal Caldiscount = Decimal.valueOf(surcharge.substring(1));
                        Decimal Caldiscount1 = pro.UnitPrice*Caldiscount;
                       //months = Math.round((pro.UnitPrice-Caldiscount1));
                         months = (pro.UnitPrice-Caldiscount1);
                        
                    } else {
                        //months = Math.round((pro.UnitPrice*discountMap.get(discount)));
                        months = (pro.UnitPrice*discountMap.get(discount));
                    }
                    System.debug('months-------------'+months); 
                    months = roundUpCalculate(months);
                   
                    Integer discounts=Integer.valueOf(discount);
                    monthDiscountMap.put(discounts,months);
                    setStrings = monthDiscountMap.keySet();
                    keyValues =keyValues == null ? new Set<Integer>() : keyValues;
                    keyValues.addAll(setStrings);
                    //keyValues.sort();
                }
            }
            System.debug('monthDiscountMap------)'+monthDiscountMap);
        }
    }
    
    public static decimal roundUpCalculate(decimal price){
        
        price=price.setScale(1);
        integer i;
        i=(integer)price.round(system.RoundingMode.CEILING);
        System.debug('price-------------'+price);
        System.debug('i-------------'+i);
        String lastvaluesss= String.valueOf(i).right(1);
        System.debug('lastvaluesss-------'+lastvaluesss);
        if(lastvaluesss.equals('1')){
            price=Integer.valueOf(i)+9;
        }else if(lastvaluesss.equals('2')){
            price=Integer.valueOf(i)+8;
        }else if(lastvaluesss.equals('3')){
            price=Integer.valueOf(i)+7;
        }else if(lastvaluesss.equals('4')){
            price=Integer.valueOf(i)+6;
        }else if(lastvaluesss.equals('5')){
            price=Integer.valueOf(i)+5;
        }else if(lastvaluesss.equals('6')){
            price=Integer.valueOf(i)+4;
        }else if(lastvaluesss.equals('7')){
            price=Integer.valueOf(i)+3;
        }else if(lastvaluesss.equals('8')){
            price=Integer.valueOf(i)+2;
        }else if(lastvaluesss.equals('9')){
            price=Integer.valueOf(i)+1;
        }else{
            price=i;
        }
        
        return price;
    }
    
    public Static Map<Decimal,Decimal> getDiscountMap(){
        Map<Decimal,Decimal> discountMap = new Map<Decimal,Decimal>();
        //System.debug('vieolabid---------'+vieolabid);
        List<Term_Discount__c> TermDiscountList=[SELECT Name,Markup_Discount__c,Term__c,Lookup_Price__c FROM Term_Discount__c where Lookup_Price__c!=null and vieo__c=:vieolabid];
        for(Term_Discount__c TermDiscount : TermDiscountList){
            
            if(TermDiscount.Markup_Discount__c>0){
                Decimal discount=TermDiscount.Markup_Discount__c/100;
                discount=1+discount;
                //System.debug('discount else---#'+discount);
                discountMap.put(TermDiscount.Term__c,discount);
            }else if(TermDiscount.Markup_Discount__c<0){
                discountMap.put(TermDiscount.Term__c,TermDiscount.Markup_Discount__c);
            }else{
                Decimal discount=TermDiscount.Markup_Discount__c/100;
                discountMap.put(TermDiscount.Term__c,discount);
            }
            
        }
        return discountMap;
    }
    
    
    
}