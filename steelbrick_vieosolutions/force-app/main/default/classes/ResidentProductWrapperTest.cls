@istest
Public class ResidentProductWrapperTest{
    @istest static void ResidentProductWrapperTest(){
    
        Product2 objproduct2 = new product2(name= 'testproduct2',SQ_FT__c=0);
        insert objproduct2;
        ID pb = Test.getStandardPricebookId();
        pricebookEntry objpricebookEntry = new pricebookEntry(pricebook2ID=pb, product2id=objproduct2.id,UnitPrice =5.5);
        insert objpricebookEntry;
        product2 objselproduct2 = [select id, name,(select id, product2id, unitprice from pricebookentries) from product2 where id=:objproduct2.id];
        map<decimal,decimal> mapdecimal = new map<decimal,decimal>();
       	mapdecimal.put(3, -100.12);
        mapdecimal.put(12, 0);
        mapdecimal.put(24, 100.23);
        ResidentProductWrapper obj = new ResidentProductWrapper(objpricebookEntry,mapdecimal);
        //
        PriceBook2 pb2=new PriceBook2();
        pb2.Name = 'test';
        pb2.IsActive = true;
        insert pb2;
        
        Vieo__c vieo = new Vieo__c();
        vieo.Name = 'california';
        vieo.Price_Book__c = pb2.id;
        insert vieo;
         Id pricebookId = Test.getStandardPricebookId();
        Product2 pro = new Product2();
        pro.Name = 'Test 1';
        //pro.Category__c = 'Resident Memberships';
        pro.ProductCode = '100';
        pro.Description = 'For testing 1';
        pro.Number_of_Workstations__c = 5;
        pro.SQ_FT__c = 250;
        pro.Location__c = 'window';
        pro.Vieo__c = vieo.Id;
        insert pro;
         PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 3640, IsActive = true);
        insert standardPrice;
        Term_Discount__c termdiscount = new Term_Discount__c(
        Name='3 Month',Markup_Discount__c=10,Term__c=3,Lookup_Price__c=true
        );
        insert termdiscount;
        Term_Discount__c termdiscount1 = new Term_Discount__c(
        Name='12 Month',Markup_Discount__c=0,Term__c=12,Lookup_Price__c=true
        );
        insert termdiscount1;
        Term_Discount__c termdiscount2 = new Term_Discount__c(
        Name='24 Month',Markup_Discount__c=-10,Term__c=24,Lookup_Price__c=true
        );
        insert termdiscount2;
        ResidentProductWrapper.getDiscountMap();
        //
        
        
        
        ResidentProductWrapper.roundUpCalculate(2.1);
        ResidentProductWrapper.roundUpCalculate(2.2);
        ResidentProductWrapper.roundUpCalculate(2.3);
        ResidentProductWrapper.roundUpCalculate(2.4);
        ResidentProductWrapper.roundUpCalculate(2.5);
        ResidentProductWrapper.roundUpCalculate(2.6);
        ResidentProductWrapper.roundUpCalculate(2.7);
        ResidentProductWrapper.roundUpCalculate(2.8);
        ResidentProductWrapper.roundUpCalculate(2.9);
        
    }
}