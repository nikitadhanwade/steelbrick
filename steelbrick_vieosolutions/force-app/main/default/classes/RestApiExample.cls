global with sharing class RestApiExample {

    //public String pwd{get;set;}
  	//public String userName{get;set;}
    public static final String userName = 'dev2@vieosolutions.com';
    public static final String pwd = 'Vieo@1234';
    //public List<PricebookEntry> PricebookEntry{get;set;}
    public static List<PricebookEntry> PricebookEntry;
   	public static String errMsg = '';
    public static String displayError= 'none';
    public static String accessToken;
    public static String baseUrl;
    
    
   	public static String  getConnected(){
        displayError = 'none';
        errMsg = '';
   		String clientId = '3MVG98RqVesxRgQ5VEQ7zQ0ZneCoSaJaaNhgwydkh7AQ851goiVQCqIyKpxe7WhkpgQLw4OEVrnPcS0kx3gSc';
   		String clientSecret = '5413727676782997261';     
   		//String username='dev2@vieosolutions.com';
   		//String password='Vieo@1234';        
   	try{
        HttpRequest reqLogin = new HttpRequest();  
        	reqLogin.setMethod('POST');
        	reqLogin.setEndpoint('https://cs18.salesforce.com'+'/services/oauth2/token');
         	reqLogin.setBody('grant_type=password' +
                             '&client_id=' + clientId +
                             '&client_secret=' + clientSecret +
                             '&username=' + EncodingUtil.urlEncode(userName, 'UTF-8') +
                             '&password=' + EncodingUtil.urlEncode(pwd, 'UTF-8'));    
         	Http httpLogin = new Http();   
         	HTTPResponse resLogin = httpLogin.send(reqLogin);
         	Map<String, Object> jsonResponse = (Map<String, Object>)JSON.deserializeUntyped(resLogin.getBody());      
         	accessToken = (string)jsonResponse.get('access_token');
         	baseUrl    = (string)jsonResponse.get('instance_url');                
   			System.debug('******accessToken******'+accessToken);
        	System.debug('******baseUrl******'+baseUrl);
        }catch(Exception e) {
            displayError = 'block';
            System.debug('*************>'+e.getMessage());
            errMsg=e.getMessage();
    	}
        return displayError;
  	}
    public static List<PricebookEntry> getPricebook() {
        displayError = 'none';
        errMsg = '';
        PricebookEntry = new List<PricebookEntry>();
        try{
            String query = 'select id, name,UnitPrice,UseStandardPrice from PricebookEntry where name = \'Test Product Vieo\' AND UseStandardPrice = True'; 
           // passing parameter
           // String query = 'SELECT id,Name,Industry,phone  FROM  Account WHERE Name =\'GenePoint\''; 
            System.debug(query);
            
            PageReference theUrl = new PageReference(baseUrl + '/services/data/v22.0/query');
            theUrl.getParameters().put('q',query);
            System.debug('******theUrl.getUrl()******'+theUrl.getUrl());
            HttpRequest request = new HttpRequest();
            request.setEndpoint(theUrl.getUrl());
            request.setMethod('GET');
            request.setHeader('Authorization', 'OAuth '+accessToken);  
            String body = (new Http()).send(request).getBody();
            
         	System.debug('*******body*******'+body);
        	//Deserialize obtained json response
        	string str = '['+ body.substring(body.indexOf('records":[')+10,body.indexof(']}')) +']';
         	System.debug('*******str*******'+str);
           
        	PricebookEntry = (list<PricebookEntry>)JSON.deserialize(str,list<PricebookEntry>.class);
          	System.debug('*******PricebookEntry*******'+PricebookEntry);
            
        }catch(Exception ex) {
            System.debug('ex =>'+ex);
        	System.debug(ex.getMessage());
            displayError = 'block';
            errMsg=ex.getMessage();
    	}
        return PricebookEntry;
    }
    
   // @RemoteAction
	webservice static List<PricebookEntry> fetch() {
        displayError = getConnected();
        List<PricebookEntry> priceBookList = new  List<PricebookEntry>();
        if(!displayError.equals('block')){
        	 priceBookList = getPricebook();    
        }
       
        System.debug('*******priceBookList*******'+priceBookList);
        return priceBookList;
        
        
    }
    
    public static void getCallService(){
        try{
         	httpRequest req = new httpRequest();   
            req.setEndPoint(baseUrl+'/services/apexrest/customPriceCalculation');
            req.setMethod('GET');
            req.setHeader('Authorization', 'OAuth '+accessToken);   
            System.debug('******accessToken******'+accessToken);
            System.debug('******baseUrl******'+baseUrl);
             String body = (new Http()).send(req).getBody();
            
         	System.debug('*******body*******'+body);
        	//Deserialize obtained json response
        	//string str = '['+ body.substring(body.indexOf('records":[')+10,body.indexof(']}')) +']';
         	//System.debug('*******str*******'+str);
        	List<Account>accList = (list<Account>)JSON.deserialize(body,list<Account>.class);
          	System.debug('*******acc*******'+accList);
        }catch(Exception e){
            System.debug('******Exception*******'+e.getMessage());
            
        }
        
        
    }
}