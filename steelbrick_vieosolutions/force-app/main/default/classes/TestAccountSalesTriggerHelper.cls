@istest(SeeAllData=false)
public class TestAccountSalesTriggerHelper {

    @istest static void TestupdateAccountIdOnAccountSales() {
        
        List<Account> accountList = new List<Account>();
            for (Integer i = 0; i < 10; i++) {
                 Account account = new Account();
                 account.Name = 'DemoAccount';
                 account.Customer__c = String.valueOf(i);
                 accountList.add(account);
            }
        if(accountList.size()>0){
            insert accountList;
        }
        
        List<Account_Sales__c> accs = new List<Account_Sales__c>();
            for (Integer i = 0; i < 10; i++) {
              Account_Sales__c accountSales = new Account_Sales__c();
              accountSales.Name = 'Testname'+system.now().format().remove(' ');  
              accountSales.Customer__c = String.valueOf(i);
              accs.add(accountSales);
            }
        if(accs.size()>0){
             Insert accs;
        }
        System.assert(!accs.isEmpty());          
   }
     @istest static void TestupdateAccountIdOnAccountSalesNegative() {
        
        List<Account> accountList = new List<Account>();
            for (Integer i = 0; i < 10; i++) {
                 Account account = new Account();
                 account.Name = 'DemoAccount';
                 account.Customer__c = String.valueOf(i);
                 accountList.add(account);
            }
        if(accountList.size()>0){
            insert accountList;
        }
        
        List<Account_Sales__c> accs = new List<Account_Sales__c>();
            for (Integer i = 11; i < 20; i++) {
              Account_Sales__c accountSales = new Account_Sales__c();
              accountSales.Name = 'Testname'+system.now().format().remove(' ');  
              accountSales.Customer__c =String.valueOf(i);
              accs.add(accountSales);
            }
        if(accs.size()>0){
            database.SaveResult[] saveRes= Database.insert(accs, false);
        }
        System.assert(!accs.isEmpty());          
   }
    @istest static void TestupdateAccountIdOnAccountSalesEmpty() {
        
        List<Account_Sales__c> accs = new List<Account_Sales__c>();
           AccountSalesTriggerHelper.updateAccountIdOnAccountSales(accs);
        System.assert(accs.isEmpty());          
   }
}