@istest(SeeAllData=false)
public class TestContractTriggerHelper {
    
    @istest static void TestupdateContractStartEndDate() {
        
        Account account = new Account();
        account.Name = 'DemoAccount';
        insert account;
        
        List<Contract> contractList = new List<Contract>();
        Contract contract = new Contract();
        contract.AccountId = account.Id;
        contract.Status = 'Draft';
        contract.StartDate = Date.parse('05/18/2018');
        contract.ContractTerm = 36;
        contractList.add(contract);
        
        Contract contract1 = new Contract();
        contract1.AccountId = account.Id;
        contract1.Status = 'Draft';
        contract1.StartDate = Date.parse('06/18/2018');
        contract1.ContractTerm = 36;
        contractList.add(contract1);
        
        if(contractList.size()>0){
            insert contractList;
        }
        System.assert(!contractList.isEmpty());  
        
        List<SBQQ__Subscription__c> subscriptionList = new List<SBQQ__Subscription__c>();
     
            SBQQ__Subscription__c subscription = new SBQQ__Subscription__c();
            subscription.SBQQ__Contract__c = contract1.id;
            subscription.SBQQ__Account__c = account.Id;
            subscription.SBQQ__SubscriptionStartDate__c = Date.parse('6/18/2018');
            subscription.SBQQ__SubscriptionEndDate__c = Date.parse('6/17/2021');
            subscription.SBQQ__SegmentStartDate__c = Date.parse('6/18/2018');
            subscription.SBQQ__SegmentEndDate__c = Date.parse('6/17/2019');
            subscription.SBQQ__Quantity__c = 1.00;
            subscriptionList.add(subscription);
        
            SBQQ__Subscription__c subscription1 = new SBQQ__Subscription__c();
            subscription1.SBQQ__Contract__c = contract1.id;
            subscription1.SBQQ__Account__c = account.Id;
            subscription1.SBQQ__SubscriptionStartDate__c = Date.parse('6/18/2019');
            subscription1.SBQQ__SubscriptionEndDate__c = Date.parse('6/17/2021');
            subscription1.SBQQ__SegmentStartDate__c = Date.parse('6/18/2019');
            subscription1.SBQQ__SegmentEndDate__c = Date.parse('6/17/2020');
            subscription1.SBQQ__Quantity__c = 1.00;
            subscriptionList.add(subscription1);
        
            SBQQ__Subscription__c subscription2 = new SBQQ__Subscription__c();
            subscription2.SBQQ__Contract__c = contract1.id;
            subscription2.SBQQ__Account__c = account.Id;
            subscription2.SBQQ__SubscriptionStartDate__c = Date.parse('6/18/2020');
            subscription2.SBQQ__SubscriptionEndDate__c = Date.parse('6/17/2021');
            subscription2.SBQQ__SegmentStartDate__c = Date.parse('6/18/2020');
            subscription2.SBQQ__SegmentEndDate__c = Date.parse('6/17/2021');
            subscription2.SBQQ__Quantity__c = 1.00;
            subscriptionList.add(subscription2);
      
        if(subscriptionList.size()>0){
            insert subscriptionList;
        }
        System.assert(!subscriptionList.isEmpty());
        
        contract=  [SELECT Id,Status, StartDate,ContractTerm FROM contract WHERE Id = :contract1.Id];
        contract.StartDate = Date.parse('7/18/2018');
        update contract;
        system.assertEquals(Date.parse('7/18/2018'),contract.StartDate); 
    }
}