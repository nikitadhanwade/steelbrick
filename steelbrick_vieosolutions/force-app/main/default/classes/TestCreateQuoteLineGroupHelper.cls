@isTest(SeeAllData=false)
public class TestCreateQuoteLineGroupHelper {
    
    @isTest static void TestCreateQuoteLineGroups() {
        Product2 product =TestDataFactory.createProduct();
        SBQQ__Quote__c quote =TestDataFactory.createQuote();
        System.debug('***quote****'+quote);
        SBQQ__QuoteLineGroup__c quoteGroup = TestDataFactory.createQuoteLineGroup(quote);
        System.debug('****quoteGroup***'+quoteGroup);
        List<SBQQ__QuoteLine__c> quoteLineList = TestDataFactory.createQuoteLines(quote,quoteGroup,product);
        System.debug('***quoteLineList****'+quoteLineList);
        quote.SBQQ__Type__c ='Amendment';
        update quote;
        Test.startTest();
        List<SBQQ__QuoteLine__c> listOfQuoteLine = new List<SBQQ__QuoteLine__c>();
        for(integer i=0;i<5;i++){
            SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
            quoteLine.SBQQ__Product__c =product.Id;
            quoteLine.SBQQ__Quote__c = quote.Id;
            quoteLine.Line_Group__c = quoteGroup.Id;
            quoteLine.SBQQ__Quantity__c = 1;
            quoteLine.SBQQ__Number__c = 1;
            listOfQuoteLine.add(quoteLine);
        }
        insert listOfQuoteLine;
        System.debug('***listOfQuoteLine***'+listOfQuoteLine.size());
        System.assert(!listOfQuoteLine.isEmpty());
        Test.stopTest();
    }
}