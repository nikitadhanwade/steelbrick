public class TestDataFactory {
	public static String prodId;
    public static Product2 createProduct(){
        Product2 prod = new Product2(Name='TestProd', IsActive=true); 
        insert prod;
        return prod;
    }
    public static SBQQ__Quote__c createQuote(){
        
        Account acc = new Account(name='testAtCheck');
        insert acc;      
        
        Opportunity opp = new Opportunity(Name='test', StageName='Proposal/Price Quote', CloseDate=System.today(), AccountId=acc.Id, SBQQ__QuotePricebookId__c='01s36000002nwwx');      
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Primary__c = true;
        quote.SBQQ__Status__c = 'Draft';
        quote.SBQQ__Type__c ='Quote';
        quote.SBQQ__PaymentTerms__c ='30';
        quote.SBQQ__Opportunity2__c =opp.Id;
        quote.SBQQ__LineItemsGrouped__c = true;
            
        insert quote;
        return quote;
    }
    public static SBQQ__QuoteLineGroup__c createQuoteLineGroup(SBQQ__Quote__c quote){
    		SBQQ__QuoteLineGroup__c quoteGroup = new SBQQ__QuoteLineGroup__c();
            quoteGroup.Name = 'Gp1';
        	quoteGroup.SBQQ__Quote__c = quote.Id;
        	quoteGroup.SBQQ__Number__c = 1;
        insert quoteGroup;
        return quoteGroup;
    }
    public static List<SBQQ__QuoteLine__c> createQuoteLines(SBQQ__Quote__c quote , SBQQ__QuoteLineGroup__c quoteLineGroup,Product2 prod){
    		List<SBQQ__QuoteLine__c> quoteLineList = new List<SBQQ__QuoteLine__c>();
        	SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        	quoteLine.SBQQ__Product__c =prod.Id;
            quoteLine.SBQQ__Quote__c = quote.Id;
            quoteLine.SBQQ__Quantity__c = 1;
            quoteLine.SBQQ__Number__c = 1;
            quoteLine.SBQQ__Group__c = quoteLineGroup.Id;
         	quoteLineList.add(quoteLine);
        insert quoteLineList;
        return quoteLineList;
    }
    
}