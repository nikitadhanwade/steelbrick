@istest
public class UploadCSVFileTest {
    
    @testSetup static void setup() {
        
        Text_Column__mdt textCol = new Text_Column__mdt();
        
        
        SBQQ__ImportFormat__c importFormat = new SBQQ__ImportFormat__c();
        importFormat.SBQQ__Active__c=true;
        importFormat.SBQQ__ImportFormatName__c='Demo';
        insert importFormat;
        
        SBQQ__ImportColumn__c importColumn = new SBQQ__ImportColumn__c();
        importColumn.Name='Product Code';
        importColumn.Can_Group__c=true;
        importColumn.IsCheck__c=true;
        importColumn.SBQQ__ColumnIndex__c=1;
        importColumn.SBQQ__ImportFormat__c=importFormat.Id;
        insert importColumn; 
        
        
        //Instantiate the Pricebook2 record with StandardPricebookId
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        //Execute an update DML on the Pricebook2 record, to make IsStandard to true
        Update standardPricebook;
        
        Opportunity opp = new Opportunity();
        opp.name='TestOpp';
        opp.StageName='Prospecting';
        opp.CloseDate=date.today();
        insert opp;
        
        SBQQ__Quote__c quoteObj = new SBQQ__Quote__c();
        quoteObj.SBQQ__SubscriptionTerm__c=5;
        quoteObj.SBQQ__Opportunity2__c=opp.Id;
        quoteObj.SBQQ__LineItemsGrouped__c=true;
        insert quoteObj;
        
        Product2 product = new Product2();
        product.productCode='AGGE-3110-00-A01';
        product.name='AGGE-3110-00-A01';
        insert product;
        
    }
    
    
    @istest static void uploadCSVFileTest(){
        
        String csvFileBody = 'Product Code,Description,Qty,List Price,Unit Price,Disc. %,Product Class\r\n AGGE-3110-00-A01,31108TC-V48x10G6x100G or 40G QSFP+PSE,2,21845,43690,60,Cisco\r\n AGGE1,31108TCPSE,2,21845,43690,60,EMC';
        Blob csvBlobData = Blob.valueOf(csvFileBody); 
        
        SBQQ__ImportColumn__c importCol = [SELECT Id FROM SBQQ__ImportColumn__c];
        UploadCSVFile.readFile(csvBlobData);
    }
    @istest static void uploadCSVFileAsBlankValueTest(){
        
        String csvFileBody = 'Product Code,Description,Qty,List Price,Unit Price,Disc. %,Product Class\r\n "",31108TC-V48x10G6x100G or 40G QSFP+PSE,2,21845,43690,60,Cisco\r\n "",31108TC-V48x10G6x100G or 40G QSFP+PSE,2,21845,43690,60,EMC';
        Blob csvBlobData = Blob.valueOf(csvFileBody); 
        
        SBQQ__ImportColumn__c importCol = [SELECT Id FROM SBQQ__ImportColumn__c];
        UploadCSVFile.readFile(csvBlobData);
    }
    @istest static void uploadCSVFileAsZeroValueTest(){
        
        String csvFileBody = 'Product Code,Description,Qty,List Price,Unit Price,Disc. %,Product Class\r\n "",31108TC-V48x10G6x100G or 40G QSFP+PSE,2,"",43690,60,Cisco\r\n "",31108TC-V48x10G6x100G or 40G QSFP+PSE,2,"",43690,60,EMC';
        Blob csvBlobData = Blob.valueOf(csvFileBody); 
        
        SBQQ__ImportColumn__c importCol = [SELECT Id FROM SBQQ__ImportColumn__c];
        UploadCSVFile.readFile(csvBlobData);
    }
    @istest static void getRecordForLineGroupTest(){ 
        
        SBQQ__ImportFormat__c importFormat = [SELECT SBQQ__ImportFormatName__c FROM SBQQ__ImportFormat__c];
        SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        UploadCSVFile.getSfdcField(JSON.serialize(new List<string>{importFormat.SBQQ__ImportFormatName__c}),JSON.serialize(new List<string>{importCol.Name}));
        UploadCSVFile.getRecordForLineGroup(JSON.serialize(new List<string>{importCol.Name}));
        UploadCSVFile.getSelectImportFormatRecord(JSON.serialize(new List<string>{importCol.Name}));
        UploadCSVFile.getMandatoryFieldsOfImportColumns();
        UploadCSVFile.getImportFileFormats();
    }
    
    @istest static void saveRecordTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Quantity',JSON.serialize(new List<string>{'Demo'}));
    }
    @istest static void saveRecordForProductCodeTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Product Code',JSON.serialize(new List<string>{'Demo'}));
    }
    @istest static void saveRecordForDescriptionTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Description',JSON.serialize(new List<string>{'Demo'}));
    }
    @istest static void saveRecordForListPriceTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'List Price',JSON.serialize(new List<string>{'Demo'}));
    
    }
    @istest static void saveRecordForDiscTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Disc. %',JSON.serialize(new List<string>{'Demo'}));
    }
    @istest static void saveRecordForProductClassTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{'HH','TY','OO'}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Product Class',JSON.serialize(new List<string>{'Demo'}));
    }
    @istest static void saveRecordForUnitPriceTest (){
        
        //SBQQ__ImportColumn__c importCol = [SELECT name FROM SBQQ__ImportColumn__c];
        //UploadCSVFile.saveRecords(JSON.serialize(new List<string>{importCol.Qty}));
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2];
        Opportunity opp = [SELECT Id FROM Opportunity];
        SBQQ__Quote__c quoteObj = [SELECT Id FROM SBQQ__Quote__c];
        product2 product = [SELECT Id FROM product2];
         
        UploadCSVFile.saveRecords(JSON.serialize(new List<string>{'2','2','2'}),JSON.serialize(new List<string>{'AGGE-3110-00-A01','AGGE-9336-00-A01','AGGE-SFPP-00-A02'}),
                                 JSON.serialize(new List<string>{'QQ','2WW','DDD'}),JSON.serialize(new List<string>{'10','20','30'}),
                                 JSON.serialize(new List<string>{'1','2','3'}),JSON.serialize(new List<string>{'22','50','60'}), 
                                 JSON.serialize(new List<string>{'255','233','112'}),JSON.serialize(new List<string>{''}),
                                 JSON.serialize(new List<string>{''}),JSON.serialize(new List<string>{'Abc','OPP','KKK'}),quoteObj.Id,1,'Unit Price',JSON.serialize(new List<string>{'Demo'}));
    }

    
    
}