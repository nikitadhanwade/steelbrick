trigger AccountSalesTrigger on Account_Sales__c (before insert) {
    System.debug('trigeer.new'+trigger.new);
    if(trigger.isBefore){
        AccountSalesTriggerHelper.updateAccountIdOnAccountSales(trigger.new);   
    }
}