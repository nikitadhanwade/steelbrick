trigger ContractTrigger on Contract (after Update) {
    if(trigger.isAfter){
        ContractTriggerHelper.updateContractStartEndDate(trigger.newMap,trigger.oldMap);   
    }

}